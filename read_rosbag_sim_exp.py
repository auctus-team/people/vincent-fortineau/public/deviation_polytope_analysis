#!/usr/bin/env python
# %% IMPORTS 
# %matplotlib ipympl
# %matplotlib widget
%matplotlib qt5

import os
import csv
import shutil
import time
from math import floor

from bagpy import bagreader
# import matplotlib
# matplotlib.use('macosx')
import matplotlib.pyplot as plt
from matplotlib import ticker as tck
from matplotlib import rcParams
import numpy as np
from tqdm import tqdm # for progression bar
import pickle # save and load data
from copy import deepcopy
import meshcat.geometry as geom
# from tabulate import tabulate # to generate nice tables
# import sympy as sp # for latex symbols output 

from pynocchio import RobotWrapper
from pinocchio import SE3, log6, Quaternion
# import pycapacity as pycap
# import pycapacity.visual as vsl


# %% MACROS & GLOBALS
# bag_directory = "data/simulation_random_configurations_20N_23_07_26/"
# bag_directory = "data/simulation_random_configurations_20N_23_07_27/"
# bag_directory = "data/config_n64_multiple_perturbation_magnitudes/"
bag_directory = "data/config_neutral_z/"
# bag_directory = "data/config_neutral_z_bis/"

topics = ["/panda_1/franka_state_controller/joint_states", 
          "/external_force", 
          "/panda_1/desired_cartesian_pose",
          "/panda_1/franka_state_controller/Fext"]

IS_DISPLAY = False
STEP = 1e-3 # 1 ms
KP = np.diag(500*np.ones(6))
PERT_EVAL_OFFSET = 1000
SAVE_DATA = False
LOAD_DATA = True
FILE_NAME = "data/23_07_26_sim"
ROBOT_TIP_LINK = "panda_link8" # "panda_tool" # 

# %% FUNCTIONS

def plotAVGnSTD(signals, title:str='', color='red', fig=None, std_mult:int=1, plot_norm:bool=False):
  """
  Plot average and standard deviation of repeated signal.
  The signal must have the dimension: k x m x n, with k the repeatition of the signals, m the axis, and n the number of samples.
  """
  m2mm = tck.FuncFormatter(lambda x, pos: '{0:g}'.format(x*1000))
  ms2s = tck.FuncFormatter(lambda x, pos: '{0:g}'.format(x/1000))
  if plot_norm is True:
    nb_axis = 4
  else:
    nb_axis = 3
  #
  if fig is None:
    rcParams['figure.figsize'] = [12, 8]
    fig, axs = plt.subplots(1,nb_axis,sharex=True)
  else:
    axs = fig.axes
  titles = ['x', 'y', 'z', '|xyz|']
  for axis in range(0,nb_axis):
    if axis != 3:
      avg = np.mean(signals[:,axis,:], axis=0)
      std_dev = np.std(signals[:,axis,:], axis=0)
    elif axis == 3: # norm
      avg = np.mean(np.linalg.norm(signals, axis=1), axis=0)
      std_dev = np.std(np.linalg.norm(signals, axis=1), axis=0)
    #
    axs[axis].plot(avg, color=color, linewidth=2, label='mean')
    axs[axis].fill_between(list(range(len(avg))), avg - std_mult*std_dev, avg + std_mult*std_dev, color=color, alpha=0.3, label='std')
    axs[axis].yaxis.set_major_formatter(m2mm)
    axs[axis].xaxis.set_major_formatter(ms2s)
    axs[axis].title.set_text(titles[axis])

  fig.supxlabel("Time (s)")
  fig.supylabel("Deviation (mm)")
  fig.suptitle(title)
  plt.pause(0.05)
  return fig, axs

def plotConfig(sequence_names:np.ndarray, eps_list_hom_all:np.ndarray, static_errors_list_hom:np.ndarray, sim_idx:int, config:int, axe:str, ampl_list:np.ndarray, plot_norm:bool=False, shared_axis:bool=False):
  """
  For a specific config, and perturbed axis, plots multiples perturbations amplitudes deviations. The average and standard deviation of the repeated experiments are displayed as well as the simulation results with gazebo and the estimation made with the static errors.
  Args:
    - sequence_names: (m) list with the name of the experiments (m number of experimental perturbations)
    - eps_list_hom_all: (nxmx3xk) numpy list with the homegenous position deviations measured (n number of session, 3 are the x,y,z translation, k are the number of samples)
    - static_errors_list_hom: (nxmx3xk) numpy list with the homegenous static deviations estimated
    - config: integer that reference to the configuration (0 - m) 
    - axe: character specifing the axis perturbed ('x', 'y', or 'z')
    - ampl_list: list of the perturbation amplitudes (values can be listed among -25, -10, 10, 25, 40)
  """
  config_names = ["config_n"+str(config)+"_"+str(ampl)+"N_"+axe for ampl in ampl_list]
  # color_list = plt.cm.get_cmap('tab10', len(ampl_list)).colors
  color_list = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple']
  title = "config " + str(config) + " (f" + axe + " " + str(ampl_list) + "N)"

  fig = None
  for i, config_name in enumerate(config_names):
    indice = [ii for ii, string in enumerate(sequence_names) if config_name in string][0]

    selected_eps = np.array([eps_list[indice] for eps_list in eps_list_hom_all])

    plt.rcParams.update({'font.size': 16})
    fig, axs = plotAVGnSTD(selected_eps[non_sim_idx,:,:], title=title, color=color_list[i], fig=fig, std_mult=1, plot_norm=plot_norm)

    # Add the gazebo simulation result, and the prediction
    for axis in range(3):
      axs[axis].plot(selected_eps[:,axis], label="gazebo " + str(ampl_list[i]), color=color_list[i], linestyle='dashed')
      axs[axis].plot(static_errors_list_hom[indice,axis], label="pred " + str(ampl_list[i]), color=color_list[i], linestyle='dotted')
      # axs[axis].plot(PERT_EVAL_OFFSET+250, static_errors_list_hom[indice, axis, PERT_EVAL_OFFSET+250], "*", color=color_list[i], markersize=10)
    if plot_norm:
      axs[3].plot(np.linalg.norm(selected_eps[sim_idx],axis=0), label="gazebo " + str(ampl_list[i]), color=color_list[i], linestyle='dashed')
      axs[3].plot(np.linalg.norm(static_errors_list_hom[indice],axis=0), label="pred " + str(ampl_list[i]), color=color_list[i], linestyle='dotted')
      # axs[3].plot(PERT_EVAL_OFFSET+250, np.linalg.norm(static_errors_list_hom[indice, :, PERT_EVAL_OFFSET+250], axis=0), "*", color=color_list[i], markersize=10)

  legend_lines = [plt.Line2D([0], [0], color='black', linestyle='-'),
                  plt.Line2D([0], [0], color='black', linestyle='dashed'),
                  plt.Line2D([0], [0], color='black', linestyle='dotted'),
                  plt.Rectangle((0, 0), 1, 1, fc='black', alpha=0.3)]
  legend_labels = ['avg', 'gazebo', 'pred', 'std']
  axs[0].legend(legend_lines, legend_labels) 

  if shared_axis:
    y_max = max(ax.get_ylim()[1] for ax in axs)
    y_min = min(ax.get_ylim()[0] for ax in axs)
    for ax in axs:
      ax.set_ylim(1.05*y_min, 1.05*y_max)

  fig.tight_layout()

  return fig

def plotConfigNoSim(sequence_names:np.ndarray, eps_list_hom_all:np.ndarray, static_errors_list_hom:np.ndarray, config:int, axe:str, ampl_list:np.ndarray, plot_norm:bool=False, shared_axis:bool=False):
  """
  For a specific config, and perturbed axis, plots multiples perturbations amplitudes deviations. The average and standard deviation of the repeated experiments are displayed as well as the simulation results with gazebo and the estimation made with the static errors.
  Args:
    - sequence_names: (m) list with the name of the experiments (m number of experimental perturbations)
    - eps_list_hom_all: (nxmx3xk) numpy list with the homegenous position deviations measured (n number of session, 3 are the x,y,z translation, k are the number of samles)
    - static_errors_list_hom: (nxmx3xk) numpy list with the homegenous static deviations estimated
    - sim_idx: index of the gazebo simulation among the sessions n
    - config: integer that reference to the configuration (0 - 4) 
    - axe: character specifing the axis perturbed ('x', 'y', or 'z')
    - ampl_list: list of the perturbation amplitudes (values can be listed among -25, -10, 10, 25, 40)
  """
  config_name = ["config_n"+str(config)+"_"+str(ampl)+"N_"+axe for ampl in ampl_list][0]
  # color_list = plt.cm.get_cmap('tab10', len(ampl_list)).colors
  color_list = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple']
  title = "config " + str(config) + " (f" + axe + " " + str(ampl_list) + "N)"

  if plot_norm is True:
    nb_axis = 4
  else:
    nb_axis = 3
  #
  rcParams['figure.figsize'] = [12, 8]
  fig, axs = plt.subplots(nb_axis,1,sharex=True)

  indice = [ii for ii, string in enumerate(sequence_names) if config_name in string][0]
  selected_eps = eps_list_hom_all[indice]
  selected_pred_eps = static_errors_list_hom[indice]

  # Add the gazebo simulation result, and the prediction
  for axis in range(3):
    axs[axis].plot(selected_eps[axis], label="sim " + str(ampl_list[0]), color=color_list[0], linestyle='dotted')
    axs[axis].plot(selected_pred_eps[axis], label="pred " + str(ampl_list[0]), color=color_list[0], linestyle='dashed')
  if plot_norm:
    axs[3].plot(np.linalg.norm(selected_eps,axis=0), label="sim " + str(ampl_list[0]), color=color_list[0], linestyle='dotted')
    axs[3].plot(np.linalg.norm(selected_pred_eps,axis=0), label="pred " + str(ampl_list[0]), color=color_list[0], linestyle='dashed')

  legend_lines = [plt.Line2D([0], [0], color='black', linestyle='-'),
                  plt.Line2D([0], [0], color='black', linestyle='dotted')]
  legend_labels = ['sim', 'pred']
  axs[-1].legend(legend_lines, legend_labels) 

  legend_lines = [plt.Line2D([0], [0], color=color_i, linestyle='-') for color_i in color_list]
  legend_labels = ["f="+str(ampl)+"N" for ampl in ampl_list]
  axs[0].legend(legend_lines, legend_labels) 


  fig.tight_layout()

  return fig

def set_axes_equal(ax):
    """
    Make axes of 3D plot have equal scale so that spheres appear as spheres,
    cubes as cubes, etc.

    Input
      ax: a matplotlib axis, e.g., as output from plt.gca().
    """

    x_limits = ax.get_xlim3d()
    y_limits = ax.get_ylim3d()
    z_limits = ax.get_zlim3d()

    x_range = abs(x_limits[1] - x_limits[0])
    x_middle = np.mean(x_limits)
    y_range = abs(y_limits[1] - y_limits[0])
    y_middle = np.mean(y_limits)
    z_range = abs(z_limits[1] - z_limits[0])
    z_middle = np.mean(z_limits)

    # The plot bounding box is a sphere in the sense of the infinity
    # norm, hence I call half the max range the plot radius.
    plot_radius = 0.5*max([x_range, y_range, z_range])

    ax.set_xlim3d([x_middle - plot_radius, x_middle + plot_radius])
    ax.set_ylim3d([y_middle - plot_radius, y_middle + plot_radius])
    ax.set_zlim3d([z_middle - plot_radius, z_middle + plot_radius])

def computeStaticError(robot:RobotWrapper, q:np.ndarray, KP:np.ndarray, fext:np.ndarray):
  """
    .. math:: \ddot{\epsilon}^* = \ddot{x}^* - \ddot{x} = -K_D\dot{\epsilon}^* - K_P\epsilon^* + J(q) A(q)^{-1} J(q)^t f_{ext}
  Args:
    robot: robot wrapper
    q: robot joint configuration (numpy.ndarray)
    KP: proportionnal gain matrix (numpy.ndarray)
    fext: external force (only forces, not torques)
  """
  J = robot.jacobian(q)[:3,:]
  M = np.linalg.inv(KP).dot(J).dot(np.linalg.inv(robot.mass_matrix(q))).dot(J.T)
  return M.dot(fext)

# %% Data extraction

if LOAD_DATA is False:

  sequence_name = []
  force_list = []
  eps_list = []
  eps_pred_list = []
  eval_idx = []
  time_list = []
  q_list = []
  limits_reached = []

  bag_files = np.array([file for file in os.listdir(bag_directory) if file.startswith("config_n")])

  for file_nb, bag_file_name in enumerate(tqdm(bag_files, desc="Reading experiments in progress...", leave=True, position=0, miniters=5)):

    try:
      bag_file = bagreader(os.path.join(bag_directory, bag_file_name), verbose=False)
    except:
      print(os.path.join(bag_directory, bag_file_name))
      file_exists = os.path.exists(os.path.join(bag_directory, bag_file_name))
      print(file_exists)
      bag_file = bagreader(os.path.join(bag_directory, bag_file_name), verbose=False)

    # create csv from rosbag
    joint_state_msg = bag_file.message_by_topic(topics[0])
    external_force_msg = bag_file.message_by_topic(topics[1])
    desired_pose_msg = bag_file.message_by_topic(topics[2])  
    # read csv
    raw_data_force = np.genfromtxt(external_force_msg, delimiter=',', names=True, deletechars='')
    raw_data_joint = np.genfromtxt( ("\t".join(i) for i in csv.reader(open(joint_state_msg))), delimiter="\t", names=True )
    raw_data_des_pose = np.genfromtxt(desired_pose_msg, delimiter=',', names=True, deletechars='')
    # dir_name = os.path.basename(os.path.normpath(os.path.dirname(joint_state_msg)))
    shutil.rmtree(os.path.dirname(joint_state_msg)) # delete the folder created with the csv

    # get force torque data
    ft_time = np.array([data[0] for data in raw_data_force])
    force = np.array([np.array(data.tolist())[5:8] for data in raw_data_force]).T
    torque = np.array([np.array(data.tolist())[8:11] for data in raw_data_force]).T
    # get joint data
    q_time = np.array([np.array(data.tolist())[0] for data in raw_data_joint])
    q = np.array([np.array(data.tolist())[6:13] for data in raw_data_joint]).T
    # get target pose data
    x_d_time = np.array([np.array(data.tolist())[0] for data in raw_data_des_pose])
    position_des = np.array([np.array(data.tolist())[5:8] for data in raw_data_des_pose]).T
    orientation_des = np.array([np.array(data.tolist())[8:13] for data in raw_data_des_pose]).T

    # data interpolation
    t = np.arange(min(np.concatenate((ft_time, q_time))), max(np.concatenate((ft_time, q_time))), STEP)
    force = np.array([np.interp(t, ft_time, data) for data in force])
    torque = np.array([np.interp(t, ft_time, data) for data in torque])
    q = np.array([np.interp(t, q_time, data) for data in q])

    position_des = np.array([np.interp(t, x_d_time, data) for data in position_des])
    orientation_des = np.array([np.interp(t, x_d_time, data) for data in orientation_des])

    # load robot model
    panda = RobotWrapper(ROBOT_TIP_LINK, urdf_path='models/urdf/panda.urdf')

    # check for joint limits
    q_max = np.maximum.reduce(q.T)
    q_min = np.minimum.reduce(q.T) 
    q_max_reached = any(panda.q_max <= q_max)
    q_min_reached = any(panda.q_min >= q_min)
    limits_reached.append(any([q_max_reached, q_min_reached]))

    # compute poses
    pose = [SE3(panda.forward(data)) for data in q.T]
    target_pose = [SE3(Quaternion(np.array(wi)).toRotationMatrix(), np.array(ti)) for ti, wi in zip(position_des.T, orientation_des.T)]

    # compute servoing errors
    eps = []
    for pose_i, target_pose_i in zip(pose,target_pose):
      adj = np.concatenate( (np.concatenate( (pose_i.rotation, np.zeros((3,3))), axis=1 ),np.concatenate( (np.zeros((3,3)), pose_i.rotation), axis=1 )), axis=0 )
      eps.append(adj@log6(pose_i.actInv(target_pose_i)).vector)
    eps = np.array(eps).T
    # estimate servoing errors
    eps_pred = np.array([computeStaticError(panda, qi, KP[:3,:3], fi) for qi, fi in zip(q.T, force.T)]).T

    # plt.figure
    # plt.plot(np.linalg.norm(eps_pred, axis=0), color='tab:orange')

    # Compute errors between measure and estimation
    # Getting first edge index of the force perturbation
    pert_idx = np.where(np.abs(np.diff(force))>0)[1][0]
    # err convergence
    final_idx = np.nan
    err_i_old = np.zeros(3)
    # for idx, err_i in enumerate(err.T):
    #   conv_crit = np.array(abs((err_i - err_i_old)/err_i))
    #   err_i_old = err_i
    #   if all(conv_crit <= 1e-3):
    #     final_error = err_i
    #     final_idx = idx
    #     break
    # n = 10 # window size
    # for j in range(eps.shape[1] - n):
    #   conv_crit = np.sqrt( np.sum(np.array([np.square(eps[:,i] - np.mean(eps[:,j:j+n])) for i in range(j,j+n)]), axis=0)/(n-1) )


    # err = eps[0:3,pert_idx-1:] - static_errors[:,pert_idx-1:]
    # final_error = err[:,PERT_EVAL_OFFSET]
    # 
    # err_list.append(final_error)
    
    # q_list.append(q)
    force_list.append(force)
    eps_list.append(eps) 
    eps_pred_list.append(eps_pred)
    eval_idx.append(pert_idx+PERT_EVAL_OFFSET)
    sequence_name.append(bag_file_name[:-24])
    time_list.append(t)

  for eps_pred in eps_pred_list:
    plt.figure
    plt.plot(np.linalg.norm(eps_pred, axis=0), color='tab:blue', alpha=0.05)

# %% save data

if SAVE_DATA:
  data = {
    'sequence_name': sequence_name,
    'force_list': force_list,
    'eps_list': eps_list,
    'eps_pred_list': eps_pred_list,
    'eval_idx': eval_idx,
    'limits_reached': limits_reached,
    'time_list': time_list,
    'bag_directory': bag_directory,
    'STEP': STEP,
    'KP': KP,
    'PERT_EVAL_OFFSET': PERT_EVAL_OFFSET,
  } 
  with open(FILE_NAME + '.pkl', 'wb') as file:
    pickle.dump(data, file)

# %% load data

if LOAD_DATA is True:
  with open(FILE_NAME + '.pkl', 'rb') as file:
      loaded_data = pickle.load(file)

  sequence_name = loaded_data['sequence_name']
  force_list = loaded_data['force_list']
  eps_list = loaded_data['eps_list']
  eps_pred_list = loaded_data['eps_pred_list']
  eval_idx = loaded_data['eval_idx']
  limits_reached = loaded_data['limits_reached']
  time_list = loaded_data['time_list']
  bag_directory = loaded_data['bag_directory']
  STEP = loaded_data['STEP']
  KP = loaded_data['KP']
  PERT_EVAL_OFFSET = loaded_data['PERT_EVAL_OFFSET']

# %% converting to np array
eval_idx = np.array(eval_idx)
sequence_name = np.array(sequence_name)

# %% Reordering the data according to the configuration
force_list_tmp = deepcopy(force_list)
eps_list_tmp = deepcopy(eps_list)
eps_pred_list_tmp = deepcopy(eps_pred_list) # already in the same order as simulation
eval_idx_tmp = deepcopy(eval_idx)
time_list_tmp = deepcopy(time_list)
limits_reached_tmp = deepcopy(limits_reached)

idx_sorted = np.argsort(sequence_name)
force_list = [deepcopy(force_list_tmp[idx]) for idx in idx_sorted]
eps_list = [deepcopy(eps_list_tmp[idx]) for idx in idx_sorted]
eval_idx = [deepcopy(eval_idx_tmp[idx]) for idx in idx_sorted]
time_list = [deepcopy(time_list_tmp[idx]) for idx in idx_sorted]
eps_pred_list = [deepcopy(eps_pred_list_tmp[idx]) for idx in idx_sorted]
limits_reached = [deepcopy(limits_reached_tmp[idx]) for idx in idx_sorted]
sequence_name = sorted(sequence_name)

del force_list_tmp, eps_list_tmp, eval_idx_tmp, time_list_tmp, limits_reached_tmp

# %% all configuration don't have perturbations on the 3 axes
# for name in sequence_name[:,:,3]:
idx_del = []
prev_config_base_name = ""
for i, sequence_i in enumerate(sequence_name):
  #
  config_base_name = sequence_i[:-1]
  if prev_config_base_name == config_base_name:
    continue

  for j in range(3):
    if i+j < len(sequence_name):
      if not (config_base_name == sequence_name[i+j][:-1]):
        # print('error ' + config_base_name)
        idx_del.append([i for i, name in enumerate(sequence_name) if config_base_name in name])
        break
    else:
      # print('error for ' + config_base_name)
      idx_del.append([i for i, name in enumerate(sequence_name) if config_base_name in name])
      break

  prev_config_base_name = config_base_name

# idx_del = np.reshape(idx_del, (-1,))
idx_del = [item for sublist in idx_del for item in sublist]

for index in sorted(idx_del, reverse=True):
    del sequence_name[index]
    del force_list[index]
    del eps_list[index]
    del eval_idx[index]
    del time_list[index]
    del eps_pred_list[index]
    del limits_reached[index]
sequence_name = np.array(sequence_name)

# %% computing the estimation err
final_err_list = [] # err between measure and estimation
initial_err_list = [] # servoing error
final_eps_norm_list = [] # displacement norm (static value)
final_eps_norm_wo_serv_list = [] # displacement norm (static value), without servoing error
final_pred_eps_norm_list = [] # estimated displacement norm (static value)
err_norm_list = [] # err norm between measure and estimation
err_norm_wo_serv_list = [] # without the servoing error

for exp in range(len(eps_list)):
  # idx
  eval_idx_sim = eval_idx[exp]
  # errors
  initial_err_list.append(np.mean(eps_list[exp][0:3,0:500],axis=1)) # predicted error is null at the beginning
  
  final_err_list.append(eps_list[exp][0:3,eval_idx_sim] - eps_pred_list[exp][:,eval_idx_sim]) 
  final_eps_norm_list.append(np.linalg.norm(eps_list[exp][0:3,eval_idx_sim]))
  final_eps_norm_wo_serv_list.append(np.linalg.norm(eps_list[exp][0:3,eval_idx_sim] - initial_err_list[-1]))
  final_pred_eps_norm_list.append(np.linalg.norm(eps_pred_list[exp][:,eval_idx_sim]))
  err_norm_list.append(np.linalg.norm(final_err_list[-1]))
  err_norm_wo_serv_list.append(np.linalg.norm(final_err_list[-1] - initial_err_list[-1]))

final_err_list = np.array(final_err_list)
initial_err_list = np.array(initial_err_list)
final_eps_norm_list = np.array(final_eps_norm_list)
final_eps_norm_wo_serv_list = np.array(final_eps_norm_wo_serv_list)
final_pred_eps_norm_list = np.array(final_pred_eps_norm_list)
err_norm_list = np.array(err_norm_list)
err_norm_wo_serv_list = np.array(err_norm_wo_serv_list)

# %% making time data homogenous data
force_list_hom = []
eps_list_hom = []
for exp, force in enumerate(force_list):
  idx_pert = eval_idx[exp]
  force_list_hom.append(force[:,idx_pert-PERT_EVAL_OFFSET-250:idx_pert+1250])
  eps_list_hom.append(eps_list[exp][:3,idx_pert-PERT_EVAL_OFFSET-250:idx_pert+1250])

eps_pred_list_hom = []
for exp, eps_pred in enumerate(eps_pred_list):
  idx_pert = eval_idx[exp]
  eps_pred_list_hom.append(np.array(eps_pred)[:,idx_pert-PERT_EVAL_OFFSET-250:idx_pert+1250])

force_list_hom = np.array(force_list_hom)
eps_list_hom = np.array(eps_list_hom)
eps_pred_list_hom = np.array(eps_pred_list_hom)

# %% plot all perturbations for a single configuration
if bag_directory == "data/config_neutral_z/":
  data = []
  magnitude_list = [-20, 20]
  axis = "z"
  

# %% plot all perturbations for a single configuration
if bag_directory == "data/config_n64_multiple_perturbation_magnitudes/":
  data = []
  config = 64
  magnitude_list = [-10, -25, -40, 10, 25, 40]
  nb_axis = 3
  sign = 1 # for signed norm
  #
  color_list = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:cyan']
  #
  fig, axs = plt.subplots(1,nb_axis,sharex=True)
  rcParams['figure.figsize'] = [12, 8]
  m2mm = tck.FuncFormatter(lambda x, pos: '{0:g}'.format(x*1000))
  ms2s = tck.FuncFormatter(lambda x, pos: '{0:g}'.format(x/1000))
  title = "config " + str(config) + " (" + str(magnitude_list) + "N)"  
  #
  tot = 0
  for j, axe in enumerate(['x', 'y', 'z']):
    config_names = ["config_n"+str(config)+"_"+str(ampl)+"N_"+axe for ampl in magnitude_list]
    for i, config_name in enumerate(config_names):
      tot = tot+1
      #
      indice = [ii for ii, string in enumerate(sequence_name) if config_name in string][0]
      selected_eps = eps_list_hom[indice]
      selected_pred_eps = eps_pred_list_hom[indice]

      # gazebo simulation and prediction
      if '-' in config_name:
        sign = -1
      else:
        sign = 1
      axs[j].plot(sign*np.linalg.norm(selected_eps, axis=0), label="sim " + str(magnitude_list[i]), color=color_list[i], linestyle='-')
      axs[j].plot(sign*np.linalg.norm(selected_pred_eps, axis=0), label="pred " + str(magnitude_list[i]), color=color_list[i], linestyle='dashed')
      axs[j].title.set_text('f' + axe + ' perturbation')
      axs[j].yaxis.set_major_formatter(m2mm)
      axs[j].xaxis.set_major_formatter(ms2s)

      data.append(sign*np.linalg.norm(selected_eps[:,::10], axis=0))
      data.append(sign*np.linalg.norm(selected_pred_eps[:,::10], axis=0))

  fig.suptitle(title) 
  legend_lines = [plt.Line2D([0], [0], color='black', linestyle='-'),
                  plt.Line2D([0], [0], color='black', linestyle='dashed')]
  legend_labels = ['sim', 'pred']
  axs[-1].legend(legend_lines, legend_labels) 

  legend_lines = [plt.Line2D([0], [0], color=color_i, linestyle='-') for color_i in color_list]
  legend_labels = ["f="+str(ampl)+"N" for ampl in magnitude_list]
  axs[0].legend(legend_lines, legend_labels) 

  fig.tight_layout()

data = np.array(data)
headers = ",".join(['sim_f'+axe+str(magn)+',pred_f'+axe+str(magn) for axe in ['x', 'y', 'z'] for magn in magnitude_list])
np.savetxt("test.csv", data.T, delimiter=',', header=headers, comments='')

# %% plot std and mean of a repeated perturbation with positive magnitudes
ampl_list = [-20] # -25, -10, 10, 25, 40 (chose 3 values)
axes = ["x", "y", "z"]
fig = [[None for _ in range(3)] for _ in range(len(sequence_name))]
for config in [0, 3]:
  for axe in range(3):
    fig[config][axe] = plotConfigNoSim(sequence_name, eps_list_hom, eps_pred_list_hom, config, axes[axe], ampl_list, plot_norm=True)

# %%
diff_x_list = []
diff_y_list = []
diff_z_list = []
diff_limits_list = []
fig, axs = plt.subplots(3,1,sharex=True)
for ii, (eps_hom, eps_pred_hom) in enumerate(zip(eps_list_hom, eps_pred_list_hom)):
  norm_eps = np.linalg.norm(eps_pred_hom, axis=0)
  norm_pred = np.linalg.norm(eps_hom, axis=0)
  diff = norm_pred - norm_eps
  if limits_reached[ii]:
    color = 'tab:orange'
    diff_limits_list.append(diff)
  else:
    color = 'tab:blue'
  if 'x' in sequence_name[ii]:
    axs[0].plot(diff, color=color, alpha=0.01)
    diff_x_list.append(diff)
  if 'y' in sequence_name[ii]:
    axs[1].plot(diff, color=color, alpha=0.01)
    diff_y_list.append(diff)
  if 'z' in sequence_name[ii]:
    axs[2].plot(diff, color=color, alpha=0.01)
    diff_z_list.append(diff)

titles = ["fx input", "fy input", "fz input"]
m2mm = tck.FuncFormatter(lambda x, pos: '{0:g}'.format(x*1000))
ms2s = tck.FuncFormatter(lambda x, pos: '{0:g}'.format(x/1000))

for ii, diff in enumerate([diff_x_list, diff_y_list, diff_z_list]):
  axs[ii].plot(np.percentile(diff, 50, axis=0), color='black', linewidth=2, linestyle='dashed')
  axs[ii].fill_between(list(range(len(diff[0]))), np.percentile(diff, 75, interpolation='higher', axis=0), np.percentile(diff, 25, interpolation='lower', axis=0), color='black', alpha=0.4, label='iqe')
  axs[ii].fill_between(list(range(len(diff[0]))), np.percentile(diff, 95, interpolation='higher', axis=0), np.percentile(diff, 5, interpolation='lower', axis=0), color='black', alpha=0.2, label='iqe')
  axs[ii].title.set_text(titles[ii])
  axs[ii].yaxis.set_major_formatter(m2mm)
  axs[ii].xaxis.set_major_formatter(ms2s)
  # axs[ii].plot(np.percentile(diff, 75, interpolation='higher', axis=0), color='black', linestyle='dashed')
  # axs[ii].plot(np.percentile(diff, 25, interpolation='lower', axis=0), color='black', linestyle='dashed')

fig.supxlabel("Time (s)")
fig.supylabel("Error (mm)")
fig.suptitle("Estimation error between simulation and prediction")

# %%
