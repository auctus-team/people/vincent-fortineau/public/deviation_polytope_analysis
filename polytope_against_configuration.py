# %%
%matplotlib qt

import os

import numpy as np
from scipy.spatial import ConvexHull
from decimal import Decimal
import matplotlib.pyplot as plt

import pynocchio as pynoc
import pinocchio as pin

import pycapacity as pycap
import pycapacity.visual as pycapviz
import pycapacity.algorithms as pycapalg
import pycapacity.robot as pycaprob

import meshcat.geometry as g 

# %%

dir_path = os.path.dirname(os.path.abspath(pynoc.__file__))
panda = pynoc.RobotWrapper(urdf_path= dir_path+"/models/urdf/panda.urdf", mesh_path=dir_path+"/models", q=np.zeros(7))

# initial joint position
q = (panda.q_min+panda.q_max)/2

# %%
# define multiple configuration
q_rand = np.array([np.random.uniform(qi_min, qi_max, size=(100000)) for qi_min, qi_max in zip(panda.q_min, panda.q_max)]).T
# %% polytope volume computation for endpoint impedance control
KP = np.diag([1,2,3])
volumes_endpoint_ctrl = []
eccentricity_endpoint_ctrl = []
for qi in q_rand:
    J = panda.jacobian(qi)[:3,:] # 3D jacobian
    M = np.linalg.inv(KP) @ J @ np.linalg.inv(panda.mass_matrix(qi)) @ J.T
    # polytope vertices computation
    H, d = pycapalg.hyper_plane_shift_method(M, -1*np.ones((3,1)), np.ones((3,1)))
    vertices, faces_index = pycapalg.hspace_to_vertex(H, d)
    # polytope volume computation
    volumes_endpoint_ctrl.append(ConvexHull(vertices.T, incremental=True).volume)
    # eccentricity ratio computation
    l, _ = np.linalg.eig(M)
    l = np.sort(l)
    ratio_1 = l[-1]/l[0]
    ratio_2 = l[-1]/l[1]
    eccentricity_endpoint_ctrl.append([ratio_1, ratio_2])

print('Endpoint impedance control:')
volumes_endpoint_ctrl = np.array(volumes_endpoint_ctrl)
print("Volume = {:.3f} +/- {:.3f}".format(np.mean(volumes_endpoint_ctrl), np.std(volumes_endpoint_ctrl)))

eccentricity_endpoint_ctrl = np.array(eccentricity_endpoint_ctrl)
print("Eccentricity ratio 1 = {:.3f} +/- {:.3f}".format(np.mean(eccentricity_endpoint_ctrl[:,0]), np.std(eccentricity_endpoint_ctrl[:,0])))
print("Eccentricity ratio 2 = {:.3f} +/- {:.3f}".format(np.mean(eccentricity_endpoint_ctrl[:,1]), np.std(eccentricity_endpoint_ctrl[:,1])))

# %% polytope volume computation for joint impedance control
KP_q = np.diag([1,2,3,4,5,6,7])
volumes_joint_ctrl = []
eccentricity_joint_ctrl = []
for qi in q_rand:
    J = panda.jacobian(qi)[:3,:] # 3D jacobian
    M = np.linalg.inv(J@KP_q@np.linalg.pinv(J)) @ J@np.linalg.inv(panda.mass_matrix(qi))@J.T
    # polytope vertices computation
    H, d = pycapalg.hyper_plane_shift_method(M, -1*np.ones((3,1)), np.ones((3,1)))
    vertices, faces_index = pycapalg.hspace_to_vertex(H, d)
    # polytope volume computation
    volumes_joint_ctrl.append(ConvexHull(vertices.T, incremental=True).volume)
    # eccentricity ratio computation
    l, _ = np.linalg.eig(M)
    l = np.sort(l)
    ratio_1 = l[-1]/l[0]
    ratio_2 = l[-1]/l[1]
    eccentricity_joint_ctrl.append([ratio_1, ratio_2])

print('Joint impedance control:')
volumes_joint_ctrl = np.array(volumes_joint_ctrl)
print("Volume = {:.3f} +/- {:.3f}".format(np.mean(volumes_joint_ctrl), np.std(volumes_joint_ctrl)))

eccentricity_joint_ctrl = np.array(eccentricity_joint_ctrl)
print("Eccentricity ratio 1 = {:.3f} +/- {:.3f}".format(np.mean(eccentricity_joint_ctrl[:,0]), np.std(eccentricity_joint_ctrl[:,0])))
print("Eccentricity ratio 2 = {:.3f} +/- {:.3f}".format(np.mean(eccentricity_joint_ctrl[:,1]), np.std(eccentricity_joint_ctrl[:,1])))

# %% polytope volume computation for force mapping impedance control
KP = np.diag([1,2,3])
volumes_Jt_ctrl = []
eccentricity_Jt_ctrl = []
for qi in q_rand:
    J = panda.jacobian(qi)[:3,:] # 3D jacobian
    Minv = J@np.linalg.inv(panda.mass_matrix(qi))@J.T
    M = np.linalg.inv(Minv@KP) @ Minv
    # polytope vertices computation
    H, d = pycapalg.hyper_plane_shift_method(M, -1*np.ones((3,1)), np.ones((3,1)))
    vertices, faces_index = pycapalg.hspace_to_vertex(H, d)
    # polytope volume computation
    volumes_Jt_ctrl.append(ConvexHull(vertices.T, incremental=True).volume)
    # eccentricity ratio computation
    l, _ = np.linalg.eig(M)
    l = np.sort(l)
    ratio_1 = l[-1]/l[0]
    ratio_2 = l[-1]/l[1]
    eccentricity_Jt_ctrl.append([ratio_1, ratio_2])

print('Force mapping impedance control:')
volumes_Jt_ctrl = np.array(volumes_Jt_ctrl)
print("Volume = {:.3f} +/- {:.3f}".format(np.mean(volumes_Jt_ctrl), np.std(volumes_Jt_ctrl)))

eccentricity_Jt_ctrl = np.array(eccentricity_Jt_ctrl)
print("Eccentricity ratio 1 = {:.3f} +/- {:.3f}".format(np.mean(eccentricity_Jt_ctrl[:,0]), np.std(eccentricity_Jt_ctrl[:,0])))
print("Eccentricity ratio 2 = {:.3f} +/- {:.3f}".format(np.mean(eccentricity_Jt_ctrl[:,1]), np.std(eccentricity_Jt_ctrl[:,1])))

# %%
# eigen_val_list = []
# eigen_vec_list = []
# for qi in q_rand:
#     J = panda.jacobian(qi)[:3,:] # 3D jacobian
#     M = np.linalg.inv(KP) @ J @ np.linalg.inv(panda.mass_matrix(qi)) @ J.T
#     #
#     l, v = np.linalg.eig(M)
#     eigen_val_list.append(l)
#     eigen_vec_list.append(v)

# eigen_val_list = np.array(eigen_val_list)
# eigen_vec_list = np.array(eigen_vec_list)
# print("val mean: {} +/- {}".format(np.mean(eigen_val_list, axis=0), np.std(eigen_val_list, axis=0)))
# print(np.mean(eigen_vec_list, axis=0))
# print(np.std(eigen_vec_list, axis=0))

# %%
q_rand[:6,:] = np.array([[ 1.91127029,  1.08147634, -0.12854064, -1.17944114,  0.03183309,
         2.1932093 ,  1.87140121],
       [-2.205938  , -1.64437843, -0.35175433, -1.8093129 , -1.63962578,
         1.66781887,  0.94548053],
       [ 2.14552251,  0.92251038, -2.67681283, -0.97699366,  0.28281351,
         0.31775799,  0.56444908],
       [-2.89490452, -1.71271119, -1.85097137, -0.47649462, -1.42815773,
         2.32235488,  1.88261676],
       [-1.99547387,  1.63843375, -0.23128063, -3.09311244, -1.88547474,
         2.89597619,  0.533307  ],
       [ 1.61034234, -1.37442606,  0.0937574 , -3.06656862, -0.38647821,
         3.10816834, -0.81113782]])

fe_min = -0.5*np.ones((3,1))
fe_max = 0.5*np.ones((3,1))
fig = plt.figure(figsize=(15, 12))
for i, qi in enumerate(q_rand[:3,:]):
    J = panda.jacobian(qi)[:3,:] # 3D jacobian
    # computes vertices and faces of the polytope
    # polytope vertices computation for joint impedance control
    M = np.linalg.inv(J@KP_q@np.linalg.pinv(J)) @ J @ np.linalg.inv(panda.mass_matrix(qi)) @ J.T
    #
    H, d = pycapalg.hyper_plane_shift_method(M, fe_min, fe_max)
    vertices, faces_index = pycapalg.hspace_to_vertex(H, d)
    print(vertices.T)
    print(faces_index)
    faces = pycaprob.face_index_to_vertex(vertices, faces_index)
    vol = ConvexHull(vertices.T, incremental=True).volume
    # display polytope
    ax = fig.add_subplot(3, 3, 3*i+1, projection='3d')
    pycapviz.plot_polytope_vertex(plot=ax, vertex=vertices, color='blue')
    pycapviz.plot_polytope_faces(plot=ax, faces=faces, alpha=0.2, face_color='blue', edge_color='blue')
    ax.title.set_text("Jnt Vol={:.2E} $dm^3$".format(Decimal(vol*1e3)))
    # polytope vertices computation for endpoint impedance control
    M = np.linalg.inv(KP) @ J @ np.linalg.inv(panda.mass_matrix(qi)) @ J.T
    #
    H, d = pycapalg.hyper_plane_shift_method(M, fe_min, fe_max)
    vertices, faces_index = pycapalg.hspace_to_vertex(H, d)
    print(vertices.T)
    print(faces_index)
    faces = pycaprob.face_index_to_vertex(vertices, faces_index)
    vol = ConvexHull(vertices.T, incremental=True).volume
    # display polytope
    ax = fig.add_subplot(3, 3, 3*i+2, projection='3d')
    pycapviz.plot_polytope_vertex(plot=ax, vertex=vertices, color='blue')
    pycapviz.plot_polytope_faces(plot=ax, faces=faces, alpha=0.2, face_color='blue', edge_color='blue')
    ax.title.set_text("End Vol={:.2E} $dm^3$".format(Decimal(vol*1e3)))
    # polytope vertices computation for force mapping control
    M = np.linalg.inv(np.eye(3))
    #
    H, d = pycapalg.hyper_plane_shift_method(M, fe_min, fe_max)
    vertices, faces_index = pycapalg.hspace_to_vertex(H, d)
    print(vertices.T)
    print(faces_index)
    faces = pycaprob.face_index_to_vertex(vertices, faces_index)
    vol = ConvexHull(vertices.T, incremental=True).volume
    # display polytope
    ax = fig.add_subplot(3, 3, 3*i+3, projection='3d')
    pycapviz.plot_polytope_vertex(plot=ax, vertex=vertices, color='blue')
    pycapviz.plot_polytope_faces(plot=ax, faces=faces, alpha=0.2, face_color='blue', edge_color='blue')
    ax.title.set_text("J^t Vol={:.2E} $dm^3$".format(Decimal(vol*1e3)))
fig.tight_layout()

# %%

# update the robot
panda.update_joint_data(q=q)
panda.update_visualisation()

# initial joint position print
print("initial q\n", q)

# calculate the polytope
opt = {'calculate_faces':True}

# polytope material
mat_poly = g.MeshBasicMaterial(color=0x0022ff, wireframe=True, linewidth=3, opacity=0.2)
# ellipsoid material
mat_ellipse = g.MeshBasicMaterial(color=0xff5500, transparent=True, opacity=0.2)

while True:
    # some sinusoidal motion
    for i in np.sin(np.linspace(-np.pi,np.pi,200)):

        # update the joint position
        q[0] = i
        q[1] = i
        q[2] = i
        # update the robot
        panda.update_joint_data(q=q)
        # update the visualisation
        panda.update_visualisation()

        # calculate the jacobian
        J = panda.jacobian_position(q)

        # calculate the polytope
        vel_poly = pycap.robot.velocity_polytope(J, panda.dq_min, panda.dq_max,options=opt)

        # meshcat triangulated mesh
        poly = g.TriangularMeshGeometry(vertices=vel_poly.vertices.T/5 + panda.forward().translation, faces=vel_poly.face_indices)
        panda.add_geometry(object=poly,
                           name_id='poly', 
                           material=mat_poly)


        # calculate the ellipsoid
        vel_ellipsoid = pycap.robot.velocity_ellipsoid(J, panda.dq_max)
        # meshcat ellipsoid
        ellipsoid = g.Ellipsoid(radii=vel_ellipsoid.radii/5)
        panda.add_geometry(object=ellipsoid, 
                           name_id='ellipse', 
                           material= mat_ellipse,
                           transform=pin.SE3(vel_ellipsoid.rotation, panda.forward().translation))
 
        # wait a bit
        time.sleep(0.01)