#!/usr/bin/env python
# %% IMPORTS 
%matplotlib qt

import os
import csv
import shutil
import time
from math import floor

from bagpy import bagreader
import matplotlib.pyplot as plt
from matplotlib import ticker as tck
from matplotlib import rcParams
import numpy as np
from tqdm import tqdm # for progression bar
import pickle # save and load data
from copy import deepcopy
import meshcat.geometry as geom
from tabulate import tabulate # to generate nice tables
import sympy as sp # for latex symbols output 

from pynocchio import RobotWrapper
from pinocchio import SE3, log6, Quaternion
import pycapacity as pycap
# import pycapacity.visual as vsl


# %% MACROS & GLOBALS
bag_directories = [  "data/repeated_experiments/simulation_23_05_31/",
                     "data/repeated_experiments/experiment0_23_05_31/",
                     "data/repeated_experiments/experiment1_23_05_31/",
                     "data/repeated_experiments/experiment2_23_05_31/",
                     "data/repeated_experiments/experiment3_23_05_31/",
                     "data/repeated_experiments/experiment4_23_05_31/",
                     "data/repeated_experiments/experiment5_23_05_31/",
                     "data/repeated_experiments/experiment6_23_05_31/",
                     "data/repeated_experiments/experiment7_23_05_31/",
                     "data/repeated_experiments/experiment8_23_05_31/",
                     "data/repeated_experiments/experiment9_23_05_31/"]

# bag_directories = ["/home/vincent/.ros/simulation_body_wrench_23_06_19/"]

# bag_directories = ["/home/vincent/.ros/test_11"]

topics = ["/panda_1/franka_state_controller/joint_states", 
          "/external_force", 
          "/panda_1/desired_cartesian_pose",
          "/panda_1/franka_state_controller/Fext",
          "/external_force_tmp"]

IS_DISPLAY = False
DISPLAY_ALL_EXP = False
STEP = 1e-3 # 1 ms
KP = np.diag(500*np.ones(6))
PERT_EVAL_OFFSET = 1000
SAVE_DATA = False
LOAD_PKL = 1
LOAD_ROSBAG = 2
LOAD_DATA = LOAD_PKL
FILE_NAME = "data/23_05_31_experiments_wrench" # "23_06_20_body_wrench_sim" # 
ROBOT_TIP_LINK = "panda_link8" # "panda_tool" # 

# %% FUNCTIONS
def plotAVGnSTD(signals, title:str='', color='red', fig=None, std_mult:int=1, plot_norm:bool=False):
  """
  Plot average and standard deviation of repeated signal.
  The signal must have the dimension: k x m x n, with k the repeatition of the signals, m the axis, and n the number of samples.
  """
  m2mm = tck.FuncFormatter(lambda x, pos: '{0:g}'.format(x*1000))
  ms2s = tck.FuncFormatter(lambda x, pos: '{0:g}'.format(x/1000))
  if plot_norm is True:
    nb_axis = 4
  else:
    nb_axis = 3
  #
  if fig is None:
    rcParams['figure.figsize'] = [12, 8]
    fig, axs = plt.subplots(1,nb_axis,sharex=True)
  else:
    axs = fig.axes
  titles = ['x', 'y', 'z', '|xyz|']
  for axis in range(0,nb_axis):
    if axis != 3:
      avg = np.mean(signals[:,axis,:], axis=0)
      std_dev = np.std(signals[:,axis,:], axis=0)
    elif axis == 3: # norm
      avg = np.mean(np.linalg.norm(signals, axis=1), axis=0)
      std_dev = np.std(np.linalg.norm(signals, axis=1), axis=0)
    #
    axs[axis].plot(avg, color=color, linewidth=2, label='mean')
    axs[axis].fill_between(list(range(len(avg))), avg - std_mult*std_dev, avg + std_mult*std_dev, color=color, alpha=0.3, label='std')
    axs[axis].yaxis.set_major_formatter(m2mm)
    axs[axis].xaxis.set_major_formatter(ms2s)
    axs[axis].title.set_text(titles[axis])

  fig.supxlabel("Time (s)")
  fig.supylabel("Deviation (mm)")
  fig.suptitle(title)
  plt.pause(0.05)
  return fig, axs

def plotConfig(sequence_names:np.ndarray, eps_list_hom_all:np.ndarray, static_errors_list_hom:np.ndarray, sim_idx:int, config:int, axe:str, ampl_list:np.ndarray, plot_norm:bool=False, shared_axis:bool=False):
  """
  For a specific config, and perturbed axis, plots multiples perturbations amplitudes deviations. The average and standard deviation of the repeated experiments are displayed as well as the simulation results with gazebo and the estimation made with the static errors.
  Args:
    - sequence_names: (m) list with the name of the experiments (m number of experimental perturbations)
    - eps_list_hom_all: (nxmx3xk) numpy list with the homegenous position deviations measured (n number of session, 3 are the x,y,z translation, k are the number of samles)
    - static_errors_list_hom: (nxmx3xk) numpy list with the homegenous static deviations estimated
    - sim_idx: index of the gazebo simulation among the sessions n
    - config: integer that reference to the configuration (0 - 4) 
    - axe: character specifing the axis perturbed ('x', 'y', or 'z')
    - ampl_list: list of the perturbation amplitudes (values can be listed among -25, -10, 10, 25, 40)
  """
  config_names = ["config_n"+str(config)+"_"+str(ampl)+"N_"+axe for ampl in ampl_list]
  # color_list = plt.cm.get_cmap('tab10', len(ampl_list)).colors
  color_list = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple']
  title = "config " + str(config) + " (f" + axe + " " + str(ampl_list) + "N)"

  fig = None
  for i, config_name in enumerate(config_names):
    indice = [ii for ii, string in enumerate(sequence_names) if config_name in string][0]

    selected_eps = np.array([eps_list[indice] for eps_list in eps_list_hom_all])
    # selected_eps = np.array([eps_list[mapping_idx[idx,indice]] for idx, eps_list in enumerate(eps_list_hom_all)])

    non_sim_idx = [ii for ii in range(selected_eps.shape[0]) if ii != sim_idx]

    plt.rcParams.update({'font.size': 16})
    fig, axs = plotAVGnSTD(selected_eps[non_sim_idx,:3,:], title=title, color=color_list[i], fig=fig, std_mult=1, plot_norm=plot_norm)

    # Add the gazebo simulation result, and the prediction
    for axis in range(3):
      axs[axis].plot(selected_eps[sim_idx,axis], label="gazebo " + str(ampl_list[i]), color=color_list[i], linestyle='dashed')
      axs[axis].plot(static_errors_list_hom[indice,axis], label="pred " + str(ampl_list[i]), color=color_list[i], linestyle='dotted')
      # axs[axis].plot(PERT_EVAL_OFFSET+250, static_errors_list_hom[indice, axis, PERT_EVAL_OFFSET+250], "*", color=color_list[i], markersize=10)
    if plot_norm:
      axs[3].plot(np.linalg.norm(selected_eps[sim_idx,:3],axis=0), label="gazebo " + str(ampl_list[i]), color=color_list[i], linestyle='dashed')
      axs[3].plot(np.linalg.norm(static_errors_list_hom[indice,:3],axis=0), label="pred " + str(ampl_list[i]), color=color_list[i], linestyle='dotted')
      # axs[3].plot(PERT_EVAL_OFFSET+250, np.linalg.norm(static_errors_list_hom[indice, :, PERT_EVAL_OFFSET+250], axis=0), "*", color=color_list[i], markersize=10)

  legend_lines = [plt.Line2D([0], [0], color='black', linestyle='-'),
                  plt.Line2D([0], [0], color='black', linestyle='dashed'),
                  plt.Line2D([0], [0], color='black', linestyle='dotted'),
                  plt.Rectangle((0, 0), 1, 1, fc='black', alpha=0.3)]
  legend_labels = ['avg', 'gazebo', 'pred', 'std']
  axs[0].legend(legend_lines, legend_labels) 

  if shared_axis:
    y_max = max(ax.get_ylim()[1] for ax in axs)
    y_min = min(ax.get_ylim()[0] for ax in axs)
    for ax in axs:
      ax.set_ylim(1.05*y_min, 1.05*y_max)

  fig.tight_layout()

  return fig

def plotConfigNoSim(sequence_names:np.ndarray, eps_list_hom_all:np.ndarray, static_errors_list_hom:np.ndarray, sim_idx:int, config:int, axe:str, ampl_list:np.ndarray, plot_norm:bool=False, shared_axis:bool=False):
  """
  For a specific config, and perturbed axis, plots multiples perturbations amplitudes deviations. The average and standard deviation of the repeated experiments are displayed as well as the simulation results with gazebo and the estimation made with the static errors.
  Args:
    - sequence_names: (m) list with the name of the experiments (m number of experimental perturbations)
    - eps_list_hom_all: (nxmx3xk) numpy list with the homegenous position deviations measured (n number of session, 3 are the x,y,z translation, k are the number of samles)
    - static_errors_list_hom: (nxmx3xk) numpy list with the homegenous static deviations estimated
    - sim_idx: index of the gazebo simulation among the sessions n
    - config: integer that reference to the configuration (0 - 4) 
    - axe: character specifing the axis perturbed ('x', 'y', or 'z')
    - ampl_list: list of the perturbation amplitudes (values can be listed among -25, -10, 10, 25, 40)
  """
  config_names = ["config_n"+str(config)+"_"+str(ampl)+"N_"+axe for ampl in ampl_list]
  # color_list = plt.cm.get_cmap('tab10', len(ampl_list)).colors
  color_list = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple']
  title = "config " + str(config) + " (f" + axe + " " + str(ampl_list) + "N)"

  fig = None
  for i, config_name in enumerate(config_names):
    indice = [ii for ii, string in enumerate(sequence_names) if config_name in string][0]

    selected_eps = np.array([eps_list[indice] for eps_list in eps_list_hom_all])
    # selected_eps = np.array([eps_list[mapping_idx[idx,indice]] for idx, eps_list in enumerate(eps_list_hom_all)])

    non_sim_idx = [ii for ii in range(selected_eps.shape[0]) if ii != sim_idx]

    plt.rcParams.update({'font.size': 16})
    fig, axs = plotAVGnSTD(selected_eps[non_sim_idx,:,:], title=title, color=color_list[i], fig=fig, std_mult=1, plot_norm=plot_norm)

    # Add the gazebo simulation result, and the prediction
    for axis in range(3):
      axs[axis].plot(static_errors_list_hom[indice,axis], label="pred " + str(ampl_list[i]), color=color_list[i], linestyle='dotted')
    if plot_norm:
      axs[3].plot(np.linalg.norm(static_errors_list_hom[indice],axis=0), label="pred " + str(ampl_list[i]), color=color_list[i], linestyle='dotted')

  legend_lines = [plt.Line2D([0], [0], color='black', linestyle='-'),
                  plt.Line2D([0], [0], color='black', linestyle='dotted'),
                  plt.Rectangle((0, 0), 1, 1, fc='black', alpha=0.3)]
  legend_labels = ['avg', 'pred', 'std']
  axs[-1].legend(legend_lines, legend_labels) 

  legend_lines = [plt.Line2D([0], [0], color=color_i, linestyle='-') for color_i in color_list]
  legend_labels = ["f="+str(ampl)+"N" for ampl in ampl_list]
  axs[0].legend(legend_lines, legend_labels) 

  if shared_axis:
    y_max = max(ax.get_ylim()[1] for ax in axs)
    y_min = min(ax.get_ylim()[0] for ax in axs)
    for ax in axs:
      ax.set_ylim(1.05*y_min, 1.05*y_max)

  fig.tight_layout()

  return fig

def set_axes_equal(ax):
    """
    Make axes of 3D plot have equal scale so that spheres appear as spheres,
    cubes as cubes, etc.

    Input
      ax: a matplotlib axis, e.g., as output from plt.gca().
    """

    x_limits = ax.get_xlim3d()
    y_limits = ax.get_ylim3d()
    z_limits = ax.get_zlim3d()

    x_range = abs(x_limits[1] - x_limits[0])
    x_middle = np.mean(x_limits)
    y_range = abs(y_limits[1] - y_limits[0])
    y_middle = np.mean(y_limits)
    z_range = abs(z_limits[1] - z_limits[0])
    z_middle = np.mean(z_limits)

    # The plot bounding box is a sphere in the sense of the infinity
    # norm, hence I call half the max range the plot radius.
    plot_radius = 0.5*max([x_range, y_range, z_range])

    ax.set_xlim3d([x_middle - plot_radius, x_middle + plot_radius])
    ax.set_ylim3d([y_middle - plot_radius, y_middle + plot_radius])
    ax.set_zlim3d([z_middle - plot_radius, z_middle + plot_radius])

def computeStaticError(robot:RobotWrapper, q:np.ndarray, KP:np.ndarray, fext:np.ndarray):
  """
    .. math:: \ddot{\epsilon}^* = \ddot{x}^* - \ddot{x} = -K_D\dot{\epsilon}^* - K_P\epsilon^* + J(q) A(q)^{-1} J(q)^t f_{ext}
  Args:
    robot: robot wrapper
    q: robot joint configuration (numpy.ndarray)
    KP: proportionnal gain matrix (numpy.ndarray)
    fext: external force (only forces, not torques)
  """
  J = robot.jacobian(q)[:3,:]
  M = np.linalg.inv(KP).dot(J).dot(np.linalg.inv(robot.mass_matrix(q))).dot(J.T)
  return M.dot(fext)

def computeStaticErrorWrench(robot:RobotWrapper, q:np.ndarray, KP:np.ndarray, wrench_ext:np.ndarray):
  """
    .. math:: \ddot{\epsilon}^* = \ddot{x}^* - \ddot{x} = -K_D\dot{\epsilon}^* - K_P\epsilon^* + J(q) A(q)^{-1} J(q)^t f_{ext}
  Args:
    robot: robot wrapper
    q: robot joint configuration (numpy.ndarray)
    KP: proportionnal gain matrix (numpy.ndarray)
    fext: external force (only forces, not torques)
  """
  J = robot.jacobian(q)
  M = np.linalg.inv(KP).dot(J).dot(np.linalg.inv(robot.mass_matrix(q))).dot(J.T)
  return M.dot(wrench_ext)

# %% Data extraction
if LOAD_DATA == LOAD_ROSBAG:

  force_list_all = []
  eps_list_all = []
  eval_idx_all = []
  sequence_name_all = []
  time_list_all = []
  # is_body_wrench_all = []
  static_errors_list_all = []

  for bag_idx, bag_directory in enumerate(tqdm(bag_directories, desc="Reading sessions in progress...", leave=None, position=0)):

    if "simulation" in bag_directory:
      sim_idx = bag_idx
      static_errors_list = []
    # else:
    #   break
    force_list = []
    eps_list = []
    eval_idx = []
    sequence_name = []
    time_list = []
    # static_errors_list = []

    bag_files = np.array([file for file in os.listdir(bag_directory) if file.startswith("config_n")])
    # is_body_wrench = np.zeros(len(bag_files))

    for file_nb, bag_file_name in enumerate(tqdm(bag_files, desc="Reading experiments in progress...", leave=None, position=1, miniters=5)):
      try:
        bag_file = bagreader(os.path.join(bag_directory, bag_file_name), verbose=False)
      except:
        print(os.path.join(bag_directory, bag_file_name))
        if not bag_file_name.endswith('.bag'):
          bag_file_name = bag_file_name + ".bag"
          bag_file = bagreader(os.path.join(bag_directory, bag_file_name), verbose=False)
        else:
          file_exists = os.path.exists(os.path.join(bag_directory, bag_file_name))
          print(file_exists)

      # is_body_wrench[file_nb] = False
      # create csv from rosbag
      joint_state_msg = bag_file.message_by_topic(topics[0])
      external_force_msg = bag_file.message_by_topic(topics[1])
      desired_pose_msg = bag_file.message_by_topic(topics[2])  
      # if external_force_msg is None: # 
      #   external_force_msg = bag_file.message_by_topic(topics[4])  
      #   is_body_wrench[file_nb] = True
      # read csv
      raw_data_force = np.genfromtxt(external_force_msg, delimiter=',', names=True, deletechars='')
      raw_data_joint = np.genfromtxt( ("\t".join(i) for i in csv.reader(open(joint_state_msg))), delimiter="\t", names=True )
      raw_data_des_pose = np.genfromtxt(desired_pose_msg, delimiter=',', names=True, deletechars='')
      # dir_name = os.path.basename(os.path.normpath(os.path.dirname(joint_state_msg)))
      shutil.rmtree(os.path.dirname(joint_state_msg)) # delete the folder created with the csv

      # get force torque data
      ft_time = np.array([data[0] for data in raw_data_force])
      force = np.array([np.array(data.tolist())[5:8] for data in raw_data_force]).T
      torque = np.array([np.array(data.tolist())[8:11] for data in raw_data_force]).T
      # get joint data
      q_time = np.array([np.array(data.tolist())[0] for data in raw_data_joint])
      q = np.array([np.array(data.tolist())[6:13] for data in raw_data_joint]).T
      # get target pose data
      x_d_time = np.array([np.array(data.tolist())[0] for data in raw_data_des_pose])
      position_des = np.array([np.array(data.tolist())[5:8] for data in raw_data_des_pose]).T
      orientation_des = np.array([np.array(data.tolist())[8:13] for data in raw_data_des_pose]).T

      # data interpolation
      t = np.arange(min(np.concatenate((ft_time, q_time))), max(np.concatenate((ft_time, q_time))), STEP)
      force = np.array([np.interp(t, ft_time, data) for data in force])
      torque = np.array([np.interp(t, ft_time, data) for data in torque])
      wrench = np.vstack((force, torque))
      q = np.array([np.interp(t, q_time, data) for data in q])
      position_des = np.array([np.interp(t, x_d_time, data) for data in position_des])
      orientation_des = np.array([np.interp(t, x_d_time, data) for data in orientation_des])

      # load robot model
      ws_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)
      # panda = RobotWrapper(ROBOT_TIP_LINK, os.path.join(ws_path, "src/franka_ros/franka_description/panda.urdf"))
      panda = RobotWrapper(ROBOT_TIP_LINK, "models/urdf/panda.urdf")

      # compute poses
      pose = [SE3(panda.forward(data)) for data in q.T]
      target_pose = [SE3(Quaternion(np.array(wi)).toRotationMatrix(), np.array(ti)) for ti, wi in zip(position_des.T, orientation_des.T)]

      # compute servoing errors
      eps = []
      for pose_i, target_pose_i in zip(pose,target_pose):
        adj = np.concatenate( (np.concatenate( (pose_i.rotation, np.zeros((3,3))), axis=1 ),np.concatenate( (np.zeros((3,3)), pose_i.rotation), axis=1 )), axis=0 )
        eps.append(adj@log6(pose_i.actInv(target_pose_i)).vector)
      eps = np.array(eps).T
      # estimate servoing errors
      if "simulation" in bag_directory:
        if sim_idx == bag_idx:
          # static_errors = np.array([computeStaticError(panda, qi, KP[:3,:3], fi) for qi, fi in zip(q.T, force.T)]).T
          static_errors = np.array([computeStaticErrorWrench(panda, qi, KP, wri) for qi, wri in zip(q.T, wrench.T)]).T
          static_errors_list.append(static_errors)
  
      #   static_errors = np.array([computeStaticError(panda, qi, KP[:3,:3], fi) for qi, fi in zip(q.T, force.T)]).T
      #   static_errors_list.append(static_errors)

      # Compute errors between measure and estimation
      # Getting first edge index of the force perturbation
      pert_idx = np.where(np.abs(np.diff(force))>0)[1][0]
      # err convergence
      # final_idx = np.nan
      # err_i_old = np.zeros(3)
      # for idx, err_i in enumerate(err.T):
      #   conv_crit = np.array(abs((err_i - err_i_old)/err_i))
      #   err_i_old = err_i
      #   if all(conv_crit <= 1e-3):
      #     final_error = err_i
      #     final_idx = idx
      #     break

      # err = eps[0:3,pert_idx-1:] - static_errors[:,pert_idx-1:]
      # final_error = err[:,PERT_EVAL_OFFSET]
      # 
      # err_list.append(final_error)
      force_list.append(force)
      eps_list.append(eps)
      eval_idx.append(pert_idx+PERT_EVAL_OFFSET)
      sequence_name.append(bag_file_name[:-24])
      time_list.append(t)

      # print("File n°{}/{}".format(file_nb+1, bag_files.shape[0]))
    print("### Experiment n°{}/{}".format(bag_idx+1, len(bag_directories)))
    # err_list_all.append(err_list)
    force_list_all.append(force_list)
    eps_list_all.append(eps_list)
    eval_idx_all.append(eval_idx)
    sequence_name_all.append(sequence_name)
    time_list_all.append(time_list)
    # is_body_wrench_all.append(is_body_wrench)
    # if 'sim_idx' not in globals():
    #   static_errors_list_all.append(static_errors_list)

# %% save data
if SAVE_DATA:
  data = {
    'force_list_all': force_list_all,
    'eps_list_all': eps_list_all,
    'static_errors_list': static_errors_list,
    'eval_idx_all': eval_idx_all,
    'sequence_name_all': sequence_name_all,
    'time_list_all': time_list_all,
    'sim_idx': sim_idx,
    'bag_directories': bag_directories,
    'STEP': STEP,
    'KP': KP,
    'PERT_EVAL_OFFSET': PERT_EVAL_OFFSET,
    # 'is_body_wrench_all': is_body_wrench_all,
  } 
  with open(FILE_NAME + '.pkl', 'wb') as file:
    pickle.dump(data, file)

# %% load data
if LOAD_DATA is LOAD_PKL:
  with open(FILE_NAME + '.pkl', 'rb') as file:
      loaded_data = pickle.load(file)

  force_list_all = loaded_data['force_list_all']
  eps_list_all = loaded_data['eps_list_all']
  static_errors_list = loaded_data['static_errors_list']
  eval_idx_all = loaded_data['eval_idx_all']
  sequence_name_all = loaded_data['sequence_name_all']
  time_list_all = loaded_data['time_list_all']
  sim_idx = loaded_data['sim_idx']
  bag_directories = loaded_data['bag_directories']
  STEP = loaded_data['STEP']
  KP = loaded_data['KP']
  PERT_EVAL_OFFSET = loaded_data['PERT_EVAL_OFFSET']
  try:
    is_body_wrench_all = loaded_data['is_body_wrench_all']
  except:
    pass

# %% converting to np array
eval_idx_all = np.array(eval_idx_all)
sequence_name_all = np.array(sequence_name_all)
# mapping between the experimental and simulated data
# mapping_idx = np.array([(i, j) for i, x in enumerate(sequence_name_all[0]) for j, y in enumerate(sequence_name_all[1]) if x == y])
mapping_idx = np.zeros((len(sequence_name_all), len(sequence_name_all[0])), int)
for i in range(len(sequence_name_all)):
  for j in range(len(sequence_name_all[0])):
    if i != sim_idx:
      mapping_idx[i][j] = np.where(sequence_name_all[i] == sequence_name_all[sim_idx][j])[0][0]

mapping_idx[sim_idx] = np.array(range(0,len(sequence_name_all[sim_idx])))

# %% Reordeing data according to the mapping
# force_list_all_tmp = deepcopy(force_list_all)
# eps_list_all_tmp = deepcopy(eps_list_all)
# # static_errors_list_tmp = deepcopy(static_errors_list) # already in the same order as simulation
# eval_idx_all_tmp = deepcopy(eval_idx_all)
# time_list_all_tmp = deepcopy(time_list_all)

# for i, ses_map_idx in enumerate(mapping_idx):
#   force_list_all[i] = [deepcopy(force_list_all_tmp[i][idx]) for idx in ses_map_idx]
#   eps_list_all[i] = [deepcopy(eps_list_all_tmp[i][idx]) for idx in ses_map_idx]
#   # static_errors_list[i] = [deepcopy(static_errors_list_tmp[i][idx]) for idx in ses_map_idx]
#   eval_idx_all[i] = [deepcopy(eval_idx_all_tmp[i][idx]) for idx in ses_map_idx]
#   time_list_all[i] = [deepcopy(time_list_all_tmp[i][idx]) for idx in ses_map_idx]

# sequence_name = sequence_name_all[0] 
# del force_list_all_tmp, eps_list_all_tmp, eval_idx_all_tmp, time_list_all_tmp, sequence_name_all

# %% Reordering the data according to the configuration
force_list_all_tmp = deepcopy(force_list_all)
eps_list_all_tmp = deepcopy(eps_list_all)
static_errors_list_tmp = deepcopy(static_errors_list) # already in the same order as simulation
eval_idx_all_tmp = deepcopy(eval_idx_all)
time_list_all_tmp = deepcopy(time_list_all)
if 'is_body_wrench_all' in globals():
  is_body_wrench_all_tmp = deepcopy(is_body_wrench_all)

for i, sequence_name_i in enumerate(sequence_name_all):
  idx_sorted = np.argsort(sequence_name_i)
  force_list_all[i] = [deepcopy(force_list_all_tmp[i][idx]) for idx in idx_sorted]
  eps_list_all[i] = [deepcopy(eps_list_all_tmp[i][idx]) for idx in idx_sorted]
  eval_idx_all[i] = [deepcopy(eval_idx_all_tmp[i][idx]) for idx in idx_sorted]
  time_list_all[i] = [deepcopy(time_list_all_tmp[i][idx]) for idx in idx_sorted]
  if 'is_body_wrench_all' in globals():
    is_body_wrench_all[i] = [deepcopy(is_body_wrench_all_tmp[i][idx]) for idx in idx_sorted]

idx_sorted = np.argsort(sequence_name_all[sim_idx])
static_errors_list = [deepcopy(static_errors_list_tmp[idx]) for idx in idx_sorted]
sequence_name = sorted(sequence_name_all[0])

del force_list_all_tmp, eps_list_all_tmp, eval_idx_all_tmp, time_list_all_tmp, sequence_name_all
if 'is_body_wrench_all' in globals():
  del is_body_wrench_all_tmp

# %% changing sequence name for the body_wrench force perturbations
if 'is_body_wrench_all' in globals():
  for ii, is_bd_wrch in enumerate(is_body_wrench_all[0]):
    if is_bd_wrch == 1:
      sequence_name[ii] = sequence_name[ii] + "_wrench"

# %% computing the estimation err
final_err_list_all = [] # err between measure and estimation
initial_err_list_all = [] # servoing error
final_eps_norm_list_all = [] # displacement norm (static value)
final_eps_norm_wo_serv_list_all = [] # displacement norm (static value), without servoing error
final_pred_eps_norm_list_all = [] # estimated displacement norm (static value)
err_norm_list_all = [] # err norm between measure and estimation
err_norm_wo_serv_list_all = [] # without the servoing error

dim = static_errors_list[0].shape[0] # 3D or 6D (force only, or whole wrench)

for ses in range(len(eps_list_all)):
  final_err_list = [] # epsilon - predicted epsilon for the static phase (eval_idx_exp)
  initial_err_list = [] # average value of epsilon on the 100 first samples
  final_eps_norm_list = [] # norm of epsilon over the three axis values at the static phase
  final_eps_norm_wo_serv_list = [] # norm of epsilon - initial_err over the three axis values at the static phase
  final_pred_eps_norm_list = [] # norm of the predicted epsilon at the static phase
  err_norm_list = []
  err_norm_wo_serv_list = [] 
  for exp in range(len(eps_list_all[0])):
    # idx
    eval_idx_exp = eval_idx_all[ses][exp]
    eval_idx_sim = eval_idx_all[sim_idx][exp]
    # errors
    initial_err = np.mean(eps_list_all[ses][exp][0:dim,0:100],axis=1) # static error is null at the beginning
    initial_err_list.append(initial_err)
    final_err = eps_list_all[ses][exp][0:dim,eval_idx_exp] - static_errors_list[exp][:,eval_idx_exp]
    final_err_list.append(final_err) 
    final_eps_norm_list.append(np.linalg.norm(eps_list_all[ses][exp][0:3,eval_idx_exp]))
    final_eps_norm_wo_serv_list.append(np.linalg.norm(eps_list_all[ses][exp][0:3,eval_idx_exp] - initial_err[:3]))
    final_pred_eps_norm_list.append(np.linalg.norm(static_errors_list[exp][:3,eval_idx_exp]))
    err_norm_list.append(np.linalg.norm(final_err[:3]))
    err_norm_wo_serv_list.append(np.linalg.norm(final_err[:3] - initial_err[:3]))

  final_err_list_all.append(final_err_list)
  initial_err_list_all.append(initial_err_list)
  final_eps_norm_list_all.append(final_eps_norm_list)
  final_eps_norm_wo_serv_list_all.append(final_eps_norm_wo_serv_list)
  final_pred_eps_norm_list_all.append(final_pred_eps_norm_list)
  err_norm_list_all.append(err_norm_list)
  err_norm_wo_serv_list_all.append(err_norm_wo_serv_list)

final_err_list_all = np.array(final_err_list_all)
initial_err_list_all = np.array(initial_err_list_all)
final_eps_norm_list_all = np.array(final_eps_norm_list_all)
final_eps_norm_wo_serv_list_all = np.array(final_eps_norm_wo_serv_list_all)
final_pred_eps_norm_list_all = np.array(final_pred_eps_norm_list_all)
err_norm_list_all = np.array(err_norm_list_all)
err_norm_wo_serv_list_all = np.array(err_norm_wo_serv_list_all)

# %% making time data homogenous data
dim_data = np.array([[len(data[0]) for data in session] for session in force_list_all])
force_list_hom_all = []
eps_list_hom_all = []
dim = static_errors_list[0].shape[0] # 3D or 6D (force only, or whole wrench)
for ses, force_sess in enumerate(force_list_all):
  force_list_hom = []
  eps_list_hom = []
  for exp, force in enumerate(force_sess):
    idx_pert = eval_idx_all[ses][exp]
    force_list_hom.append(np.array(force)[:,idx_pert-PERT_EVAL_OFFSET-250:idx_pert+2500])
    eps_list_hom.append(eps_list_all[ses][exp][:dim,idx_pert-PERT_EVAL_OFFSET-250:idx_pert+2500])
  force_list_hom_all.append(force_list_hom)
  eps_list_hom_all.append(eps_list_hom)

force_list_hom_all = np.array(force_list_hom_all)
eps_list_hom_all = np.array(eps_list_hom_all)

static_errors_list_hom = []
for exp, static_err in enumerate(static_errors_list):
  idx_pert = eval_idx_all[sim_idx][exp]
  static_errors_list_hom.append(np.array(static_err)[:,idx_pert-PERT_EVAL_OFFSET-250:idx_pert+2500])

static_errors_list_hom = np.array(static_errors_list_hom)
eps_list_hom_all_wo_se = np.array([[[eps_list_hom_all[sess, exp, axe, :] - initial_err_list_all[sess, exp, axe] for axe in range(3)] for exp in range(60)] for sess in range(11)])

# %% plot std and mean of a repeated perturbation
if DISPLAY_ALL_EXP:
  ampl_list = [40, 25, 10, -10, -25] # -25, -10, 10, 25, 40 (chose 3 values)
  axes = ["x", "y", "z"]
  fig = [[None for _ in range(3)] for _ in range(4)]
  for config in range(4):
    for axe in range(3):
      fig[config][axe] = plotConfig(sequence_name, eps_list_hom_all, static_errors_list_hom, sim_idx, config, axes[axe], ampl_list, plot_norm=True)

# %% plot std and mean of repeated perturbations without the servoing error
if DISPLAY_ALL_EXP:
  ampl_list = [40, 25, 10, -10, -25] # -25, -10, 10, 25, 40 (chose 3 values)
  axes = ["x", "y", "z"]
  fig = [[None for _ in range(3)] for _ in range(4)]
  for config in range(4):
    for axe in range(3):
      fig[config][axe] = plotConfig(sequence_name, eps_list_hom_all_wo_se, static_errors_list_hom, sim_idx, config, axes[axe], ampl_list, plot_norm=True)

# %%  save the data from config 0 with external forces on z axis
ampl_list = [40, 25, 10, -10, -25] 
axe_f = 'z'
config = 0
selec_idx = []
config_names = ["config_n"+str(config)+"_"+str(ampl)+"N_"+axe_f for ampl in ampl_list]
for config_name in config_names:
  selec_idx.append([ii for ii, string in enumerate(sequence_name) if config_name in string][0])
#
headers_eps = ""
data_eps = []
for j, axe in enumerate(['x', 'y', 'z']):
  for i, ampl in enumerate(ampl_list):
    mean_eps = np.mean(eps_list_hom_all_wo_se[:,selec_idx[i], j, :], axis=0)
    std_eps = np.std(eps_list_hom_all_wo_se[:,selec_idx[i], j, :], axis=0)
    headers_eps = headers_eps + ",avg_" + str(ampl) + "N_" + axe
    data_eps.append(mean_eps)
    headers_eps = headers_eps + ",std_up_" + str(ampl) + "N_" + axe
    data_eps.append(mean_eps+std_eps)
    headers_eps = headers_eps + ",std_dw_" + str(ampl) + "N_" + axe
    data_eps.append(mean_eps-std_eps)
    # for k in range(1,11): # experiment repetition without the simulation (first idx)
    #   headers_eps = headers_eps + ",exp_" + str(k) + "_" +str(ampl)+"N_" + axe
    #   data_eps.append(eps_list_hom_all_wo_se[k,selec_idx[i], j, :])
    #   #
    headers_eps = headers_eps + ",pred_"+str(ampl)+"N_"+ axe
    data_eps.append(static_errors_list_hom[selec_idx[i], j, :])
    headers_eps = headers_eps + ",sim_"+str(ampl)+"N_"+ axe
    data_eps.append(eps_list_hom_all_wo_se[0,selec_idx[i], j, :])

for i, ampl in enumerate(ampl_list):
  if axe == 'z':
    norm_mean_eps = np.mean(np.linalg.norm(eps_list_hom_all_wo_se[1:,selec_idx[i], :, :], axis=1), axis=0)
    norm_std_eps = np.std(np.linalg.norm(eps_list_hom_all_wo_se[1:,selec_idx[i], :, :], axis=1), axis=0)
    norm_sim = np.linalg.norm(eps_list_hom_all_wo_se[1,selec_idx[i], :, :], axis=0)
    norm_pred = np.linalg.norm(static_errors_list_hom[selec_idx[i], :3, :], axis=0)
    headers_eps = headers_eps + ",avg_"+str(ampl)+"N_norm"
    data_eps.append(norm_mean_eps)
    headers_eps = headers_eps + ",std_up_"+str(ampl)+"N_norm"
    data_eps.append(norm_mean_eps + norm_std_eps)
    headers_eps = headers_eps + ",std_dw_"+str(ampl)+"N_norm"
    data_eps.append(norm_mean_eps - norm_std_eps)
    headers_eps = headers_eps + ",pred_"+str(ampl)+"N_norm"
    data_eps.append(norm_pred)
    headers_eps = headers_eps + ",sim_"+str(ampl)+"N_norm"
    data_eps.append(norm_sim)

data_eps = np.array(data_eps)
data_eps_sub = data_eps[:,::5]
headers_eps = headers_eps[1:] # delete the initial comma

np.savetxt("data/data_config_" + str(config) + "_f" + axe_f + ".csv", data_eps_sub.T, delimiter=',', header=headers_eps, comments='')
# plotConfig(sequence_name, eps_list_hom_all_wo_se, static_errors_list_hom, sim_idx, config, axe_f, ampl_list, plot_norm=True)

# %% compare the symmetry
config = 1 # from 0 to 4
axe = 'z' # x, y or z
ampl_list = [-25, 25] # -25, -10, 10, 25, 40 (chose 3 values)
#
fig = plotConfig(sequence_name, eps_list_hom_all, static_errors_list_hom, sim_idx, config, axe, ampl_list, plot_norm=True)
# fig.tight_layout()

# %%
session_names = []
for directory_name in bag_directories:
  index = -1
  for _ in range(4):
      index = directory_name.find('/', index + 1)
      if index == -1:
          break
  # Find the index of the next occurrence of "_"
  next_index = directory_name.index("_", index + 1)
  # Extract the part of interest
  session_names.append(directory_name[index + 1:next_index])

axes = ["X", "Y", "Z"]
fig, axs = plt.subplots(1,3,sharex=True)
for axis in range(0,3):
  for ii, final_err_list in enumerate(final_err_list_all):
    axs[axis].plot(np.array(final_err_list)[:,axis], label=session_names[ii], alpha=0.3)

  axs[axis].title.set_text(axes[axis])
axs[-1].legend()
fig.supxlabel("Exp nb")
fig.suptitle("Error statistics")

# %% Estimation error (between measured deviation and estimated deviation)
final_err_list_all_magn = final_err_list_all - initial_err_list_all
non_sim_idx = [i for i in range(final_err_list_all.shape[0]) if i != sim_idx]
fig, axs = plotAVGnSTD(np.transpose(final_err_list_all[non_sim_idx,:,:3], (0,2,1)), color='red', std_mult=3)
fig, axs = plotAVGnSTD(np.transpose(final_err_list_all_magn[non_sim_idx,:,:3], (0,2,1)), title="statistics", color='green', std_mult=3, fig=fig)

for axis in range(0,3):
  axs[axis].plot(final_err_list_all[sim_idx,:,axis], label="gazebo ", color='blue', alpha=0.7)
  axs[axis].plot(final_err_list_all_magn[sim_idx,:,axis], label="gazebo ", color='blue', alpha=0.7, linestyle='dotted')

y_max = max(ax.get_ylim()[1] for ax in axs)
y_min = min(ax.get_ylim()[0] for ax in axs)
for ax in axs:
  ax.set_ylim(1.05*y_min, 1.05*y_max)

legend_lines = [plt.Line2D([0], [0], color='black', linestyle='-'),
                plt.Rectangle((0, 0), 1, 1, fc='black', alpha=0.3),
                plt.Line2D([0], [0], color='blue', linestyle='-'),
                plt.Line2D([0], [0], color='blue', linestyle='dotted')]
legend_labels = ['avg', 'std', 'gazebo', 'gazebo-serv err']
axs[-1].legend(legend_lines, legend_labels) 

# %% Looking at the error between the predicted and the measured displacement norm
norm_err_wo = final_pred_eps_norm_list_all - final_eps_norm_list_all
norm_err_wo_serv = final_pred_eps_norm_list_all - final_eps_norm_wo_serv_list_all
avg = np.mean(norm_err_wo_serv, axis=0)
std_dev = np.std(norm_err_wo_serv, axis=0)
#
fig, ax = plt.subplots(1,1)
ax.plot(avg*1000, color='red', linewidth=2, label='mean')
ax.fill_between(list(range(len(avg))), (avg - std_dev)*1000, (avg + std_dev)*1000, color='red', alpha=0.3, label='std')
ax.title.set_text("Error of the predicted norm ($|\hat{\epsilon}| - |\epsilon|$)")
ax.set_xlabel('Experiment')
ax.set_ylabel('Error (mm)')

# %%
non_sim_idx = [i for i in range(final_err_list_all.shape[0]) if i != sim_idx]
print("Mean absolute errors: {:.3f}mm".format(np.mean(np.abs(final_err_list_all[non_sim_idx,:,:3]))*1000))
print("Standard deviation (std) absolute errors: {:.3f}mm".format(np.std(np.abs(final_err_list_all[non_sim_idx,:,:3]))*1000))
print("Mean errors: {:.3f}mm".format(np.mean(final_err_list_all[non_sim_idx,:,:3])*1000))
print("Std errors: {:.3f}mm".format(np.std(final_err_list_all[non_sim_idx,:,:3])*1000))
# the norm of the final errors
print("Mean error norm: {:.3f}mm".format(np.mean(err_norm_list_all[non_sim_idx,:])*1000))
print("Std error norm: {:.3f}mm".format(np.std(err_norm_list_all[non_sim_idx,:])*1000))
# the errors between the predicted and the measured displacement norm
print("Mean of the norm error: {:.3f}mm".format(np.mean(norm_err_wo[non_sim_idx,:])*1000))
print("Std of the norm error: {:.3f}mm".format(np.std(norm_err_wo[non_sim_idx,:])*1000))

print("Taking out the initial servoing error")
print("Mean absolute errors: {:.3f}mm".format(np.mean(np.abs(final_err_list_all[non_sim_idx,:,:3] - initial_err_list_all[non_sim_idx,:,:3]))*1000))
print("Std absolute errors: {:.3f}mm".format(np.std(np.abs(final_err_list_all[non_sim_idx,:,:3] - initial_err_list_all[non_sim_idx,:,:3]))*1000))
print("Mean errors: {:.3f}mm".format(np.mean(final_err_list_all[non_sim_idx,:,:3] - initial_err_list_all[non_sim_idx,:,:3])*1000))
print("Std errors: {:.3f}mm".format(np.std(final_err_list_all[non_sim_idx,:,:3] - initial_err_list_all[non_sim_idx,:,:3])*1000))
print("Mean error norm: {:.3f}mm".format(np.mean(err_norm_wo_serv_list_all[non_sim_idx,:])*1000))
print("Std error norm: {:.3f}mm".format(np.std(err_norm_wo_serv_list_all[non_sim_idx,:])*1000))
# the errors between the predicted and the measured displacement norm
print("Mean of the norm error: {:.3f}mm".format(np.mean(norm_err_wo_serv[non_sim_idx,:])*1000))
print("Std of the norm error: {:.3f}mm".format(np.std(norm_err_wo_serv[non_sim_idx,:])*1000))

# %% Table
disp_percent = False # display relative error in percent ?
tab_str = []
ampl_list = [40, 25, 10, -10, -25]

config_names = [[["config_n"+str(ii)+"_"+str(ampl)+"N_"+axe for axe in ['x', 'y', 'z']] for ampl in ampl_list] for ii in range(4)]
for i, config_name in enumerate(config_names):
  tab_str_row = []
  for j, ampl_name in enumerate(config_name):
    #idx = i*len(ampl_list)*3 + j*3 # 3 axis
    idxs = [ii for ii, string in enumerate(sequence_name) if string in ampl_name]
    
    # f_esp = [final_eps_norm_list_all[1:,idx]*1e3 for idx in idxs]
    f_esp = [final_eps_norm_wo_serv_list_all[1:,idx]*1e3 for idx in idxs]
    f_esp_pred = [final_pred_eps_norm_list_all[1:,idx]*1e3 for idx in idxs]
    err_percent = 100*(np.mean(f_esp_pred,axis=1) - np.mean(f_esp,axis=1))/np.mean(f_esp,axis=1)
    err = np.mean(f_esp_pred,axis=1) - np.mean(f_esp,axis=1)

    tab_str_row.append( r"\begin{{tabular}}[c]{{@{{}}l@{{}}}} \SI{{{:.2f}({:.2f})}}{{}} \\ \SI{{{:.2f}({:.2f})}}{{}} \\ \SI{{{:.2f}({:.2f})}}{{}} \end{{tabular}}".format(
      np.mean(f_esp[0]), np.std(f_esp[0]),
      np.mean(f_esp[1]), np.std(f_esp[1]),
      np.mean(f_esp[2]), np.std(f_esp[2])
    ) )
    tab_str_row.append( r"\begin{{tabular}}[c]{{@{{}}l@{{}}}} \SI{{{:.2f}}}{{}}\\\SI{{{:.2f}}}{{}}\\\SI{{{:.2f}}}{{}} \end{{tabular}}".format(
      np.mean(f_esp_pred[0]),
      np.mean(f_esp_pred[1]),
      np.mean(f_esp_pred[2])
    ) )
    if (disp_percent):
      tab_str_row.append( r"\begin{{tabular}}[c]{{@{{}}l@{{}}}} \SI{{{:.1f}}}{{}}\\\SI{{{:.1f}}}{{}}\\\SI{{{:.1f}}}{{}} \end{{tabular}}".format(
        np.mean(err_percent[0]),
        np.mean(err_percent[1]),
        np.mean(err_percent[2])
      ) )
    else:
      tab_str_row.append( r"\begin{{tabular}}[c]{{@{{}}l@{{}}}} \SI{{{:.2f}}}{{}}\\\SI{{{:.2f}}}{{}}\\\SI{{{:.2f}}}{{}} \end{{tabular}}".format(
        np.mean(err[0]),
        np.mean(err[1]),
        np.mean(err[2])
      ) )

  tab_str.append(tab_str_row)

row_headers = [r"\makecell{"+str(ii) + r"} \makecell{fx\\fy\\fz}" for ii in range(4)]
# column_headers = [r"\multicolumn{{1}}{{c}}{{\SI{{{:d}}}{{\newton}} (\si{{\milli\meter}})}}".format(ii) for ii in ampl_list]
if (disp_percent):
  column_headers = np.array([[r"avg $\pm$ std", r"sim", r"err\textsubscript{{\%}}"] for _ in ampl_list]).flatten()
else:
  column_headers = np.array([[r"avg $\pm$ std", r"sim", r"err"] for _ in ampl_list]).flatten()

print(tabulate(tab_str, headers=ampl_list, showindex=row_headers, tablefmt='fancy_grid'))
print(tabulate(tab_str, headers=column_headers, showindex=row_headers, tablefmt="latex_raw"))

# %% Displaying the displacements in polytopmics forms with the robot
config_list = [[-0.193554760, 0.3061738820, -0.44011161400, -2.80159518, -0.467341047, 2.370027970, 1.3684875100], 
               [0.0813971087, 0.8559753300, -0.25676182600, -1.83783075, -1.564609130, 1.783019210, 1.1144375000], 
               [0.2273613540, -0.595887054, -0.00156687834, -2.91068007, 1.7576110100, 0.782218358, 2.6577856700], 
               [-0.225768979, -0.724303249, -0.01719762000, -2.78464604, -1.301283810, 1.088978460, 0.5594172490]]

# ws_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)
# panda = RobotWrapper("panda_link8", os.path.join(ws_path, "src/franka_ros/franka_description/panda.urdf"), mesh_path=os.path.join(ws_path, "src/franka_ros/franka_description"), open_viewer=True)
panda = RobotWrapper("panda_link8", "models/urdf/panda.urdf", mesh_path="models/", open_viewer=True)
material = geom.MeshBasicMaterial(color=0x0022ff, wireframe=True, linewidth=3, opacity=0.2)
time.sleep(2)

for ii, q in enumerate(config_list):

  panda.update_visualisation(np.array(q))
  #
  J = panda.jacobian(q)[:3,:]
  M = np.linalg.inv(KP[:3,:3]).dot(J).dot(np.linalg.inv(panda.mass_matrix(q))).dot(J.T)
  #
  H, d = pycap.algorithms.hyper_plane_shift_method(M, -40*np.ones(3), 40*np.ones(3))
  eps_vertices, eps_faces_index = pycap.algorithms.hspace_to_vertex(H, d)
  poly = geom.TriangularMeshGeometry(vertices=eps_vertices.T + panda.forward(q).translation, faces=eps_faces_index)
  #
  panda.add_geometry(object=poly, name_id='poly', material=material)
  time.sleep(4)

# %% Displaying the displacements polytopes

titles = ['fx', 'fy', 'fz']
m2mm = tck.FuncFormatter(lambda x, pos: '{0:g}'.format(x*1000))
plt.rcParams.update({'font.size': 16})
rcParams['figure.figsize'] = [12, 8]
fig, axs = plt.subplots(2,2,subplot_kw=dict(projection='3d'))
for ii, q in enumerate(config_list):
  J = panda.jacobian(q)[:3,:]
  M = np.linalg.inv(KP[:3,:3]).dot(J).dot(np.linalg.inv(panda.mass_matrix(q))).dot(J.T)
  panda_ee = panda.forward(q).translation
  #
  H, d = pycap.algorithms.hyper_plane_shift_method(M, -40*np.ones(3), 40*np.ones(3))
  eps_vertices, eps_faces_index = pycap.algorithms.hspace_to_vertex(H, d)
  faces = pycap.robot.face_index_to_vertex(eps_vertices, eps_faces_index)

  pycap.visual.plot_polytope_vertex(plot=axs[floor(ii/2), np.mod(ii,2)], vertex=eps_vertices, label='error polytope', color='blue') # , center=panda_ee[:, None]
  pycap.visual.plot_polytope_faces(plot=axs[floor(ii/2), np.mod(ii,2)], faces=faces, alpha=0.2, face_color='blue', edge_color='blue')     

  axs[floor(ii/2), np.mod(ii,2)].xaxis.set_major_formatter(m2mm)
  axs[floor(ii/2), np.mod(ii,2)].yaxis.set_major_formatter(m2mm)    
  axs[floor(ii/2), np.mod(ii,2)].zaxis.set_major_formatter(m2mm)    
  axs[floor(ii/2), np.mod(ii,2)].title.set_text("config "+str(ii))
  axs[floor(ii/2), np.mod(ii,2)].set_box_aspect([1.0, 1.0, 1.0])
  set_axes_equal(axs[floor(ii/2), np.mod(ii,2)])

axs[1, 0].set_xlabel('x (mm)')
axs[1, 0].set_ylabel('y (mm)')
axs[1, 1].set_xlabel('x (mm)')
axs[1, 1].set_ylabel('y (mm)')
axs[1, 1].set_zlabel('z (mm)')
axs[0, 1].set_zlabel('z (mm)')

# The fix
# for spine in axs[1,1].spines.values():
#     spine.set_visible(False)

fig.tight_layout()
fig.subplots_adjust(bottom=0.08)  

# %% compare the symmetry
config = 2 # from 0 to 4
axe = 'z' # x, y or z
ampl_list = [-25, -10, 10, 25, 40] 
#
fig = plotConfig(sequence_name, eps_list_hom_all, static_errors_list_hom, sim_idx, config, axe, ampl_list, plot_norm=True)
# fig.tight_layout()
# %% Plot norm displacement for simulation and prediction (Figure x in ICRA 2024)
n_config = 1
ampl_list = [-25, -10, 10, 25, 40] 
color_list = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple']
titles = ['fx', 'fy', 'fz']

m2mm = tck.FuncFormatter(lambda x, pos: '{0:g}'.format(x*1000))
ms2s = tck.FuncFormatter(lambda x, pos: '{0:g}'.format(x/1000))
plt.rcParams.update({'font.size': 16})
rcParams['figure.figsize'] = [12, 8]
fig, axs = plt.subplots(1,3,sharex=True)

for ii, axe in enumerate(['x', 'y', 'z']):
  config_name = ["config_n"+str(n_config)+"_"+str(ampl)+"N_"+axe for ampl in ampl_list]
  indice = np.array([[ii for ii, string in enumerate(sequence_name) if string in config_name_i] for config_name_i in config_name]).squeeze()
  selected_sim_eps = np.array([eps_list[indice] for eps_list in eps_list_hom_all])[sim_idx].squeeze()
  selected_sim_eps_wo_tracking_err = np.array([[axis - np.mean(axis[:100]) for axis in exp] for exp in selected_sim_eps])
  
  for idx, (eps_pert, sim_eps_pert) in enumerate(zip(selected_sim_eps, selected_sim_eps_wo_tracking_err)):
    axs[ii].plot(np.sign(ampl_list[idx])*np.linalg.norm(eps_pert,axis=0), label="gazebo " + str(ampl_list[idx]), color=color_list[idx], linewidth=2)
    axs[ii].plot(np.sign(ampl_list[idx])*np.linalg.norm(sim_eps_pert,axis=0), label="gazebo wo te " + str(ampl_list[idx]), color=color_list[idx], linestyle='dashed', linewidth=2)
    axs[ii].plot(np.sign(ampl_list[idx])*np.linalg.norm(static_errors_list_hom[indice[idx]].squeeze(),axis=0), label="pred " + str(ampl_list[idx]), color=color_list[idx], linestyle='dotted', linewidth=2)
  
  axs[ii].yaxis.set_major_formatter(m2mm)
  axs[ii].xaxis.set_major_formatter(ms2s)
  axs[ii].title.set_text(titles[ii])
  axs[ii].xaxis.set_ticks(np.arange(0, 4000, 500))
  axs[ii].grid(True)

fig.supxlabel("Time (s)")
fig.supylabel("Deviation (mm)")
# fig.suptitle("Displacement signed norm for config n"+str(n_config))
fig.tight_layout()

legend_lines = [plt.Line2D([0], [0], color='black', linestyle='-'),
                plt.Line2D([0], [0], color='black', linestyle='dashed'),
                plt.Line2D([0], [0], color='black', linestyle='dotted')]
legend_labels = ['gazebo', 'pred', 'gazebo w/o t.e']
axs[-1].legend(legend_lines, legend_labels) 

legend_lines = [plt.Line2D([0], [0], color=color_i, linestyle='-') for color_i in color_list]
legend_labels = ["f=" + str(ampl) + "N" for ampl in ampl_list]
axs[0].legend(legend_lines, legend_labels) 

# %% Example for the same configuration of the measurements (Figure x in ICRA 2024)
axe = 'z' # x, y or z
fig = plotConfigNoSim(sequence_name, eps_list_hom_all, static_errors_list_hom, sim_idx, config, axe, ampl_list, plot_norm=True)
# fig.tight_layout()

# %% compare perturbation with body wrench and with joint torque
if FILE_NAME == "23_06_20_body_wrench_sim":
  axe = 'z' # x, y or z
  #
  config_name = "config_n0_40N_"+ axe
  color_list = ['tab:blue', 'tab:orange']

  title = "config 0 (f" + axe + " 40N)"

  fig, axs = plt.subplots(1,4,sharex=True)
  plt.rcParams.update({'font.size': 16})

  idx_t = [ii for ii, string in enumerate(sequence_name) if config_name == string][0]
  idx_w = [ii for ii, string in enumerate(sequence_name) if config_name + "_wrench"== string][0]

  eps_torque = np.array([eps_list[idx_t] for eps_list in eps_list_hom_all]).squeeze()
  eps_wrench = np.array([eps_list[idx_w] for eps_list in eps_list_hom_all]).squeeze()

  for axis in range(0,4):
    if axis != 3:
      sig_t = eps_torque[axis,:]
      sig_w = eps_wrench[axis,:]
      pred = static_errors_list_hom[idx_t,axis]
    elif axis == 3: # norm
      sig_t = np.linalg.norm(eps_torque, axis=0)
      sig_w = np.linalg.norm(eps_wrench, axis=0)
      pred = np.linalg.norm(static_errors_list_hom[idx_t,:], axis=0)
    #
    axs[axis].plot(sig_t, color=color_list[0], linewidth=2, label="joint torque")
    axs[axis].plot(sig_w, color=color_list[1], linewidth=2, label="body wrench")
    axs[axis].plot(pred, color=color_list[0], linewidth=2, linestyle='dashed', label="prediction")

  legend_lines = [plt.Line2D([0], [0], color=color_list[0], linestyle='-'),
                  plt.Line2D([0], [0], color=color_list[1], linestyle='-'),
                  plt.Line2D([0], [0], color=color_list[0], linestyle='dashed')]
  legend_labels = ['joint torque', 'body wrench', 'pred']
  axs[-1].legend(legend_lines, legend_labels)
  fig.tight_layout()

# %%
