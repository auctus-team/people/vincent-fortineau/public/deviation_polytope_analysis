#!/usr/bin/env python
# %% IMPORTS 
# %matplotlib ipympl
# %matplotlib widget
%matplotlib qt5

from math import floor

import matplotlib.pyplot as plt
from matplotlib import ticker as tck
from matplotlib import rcParams
import numpy as np
import pickle # save and load data
from copy import deepcopy 

# %% MACROS & GLOBALS
saved_files = ["data/23_07_26_sim.pkl", 
               "data/23_07_27_sim.pkl"]

ROBOT_TIP_LINK = "panda_link8" # "panda_tool" # 

# %%
# Function to modify the strings with three-figure increments
def format_increment_with_three_figures(string_list):
    formatted_list = []
    for string in string_list:
        # Split the string by '_' to extract the number part
        parts = string.split('_')
        # Extract the number part and convert it to an integer
        num = int(parts[1][1:])  # Skipping the 'n' prefix
        # Format the number with three figures using zfill and reconstruct the string
        formatted_string = f"{parts[0]}_n{str(num).zfill(3)}_{parts[2]}_{parts[3]}"
        formatted_list.append(formatted_string)
    return formatted_list

def find_intersection_indexes(*lists):
    # Find the union elements among all lists
    union_elements = set.intersection(*map(set, lists))
    return union_elements

def flatten(iterable, flattened):
    try:
        iter(iterable)
        for item in iterable:
            flatten(item, flattened)
    except:
        flattened.append(iterable)
    return flattened

# %%
loaded_data = []
for file in saved_files:
    with open(file, 'rb') as file:
        loaded_data.append(pickle.load(file))

# %% extract data
sequence_name = []
force_list = []
eps_list = []
eps_pred_list = []
eval_idx = []
limits_reached = []
time_list = []

for data in loaded_data:
    sequence_name.append(data['sequence_name'])
    force_list.append(data['force_list'])
    eps_list.append(data['eps_list'])
    eps_pred_list.append(data['eps_pred_list'])
    eval_idx.append(data['eval_idx'])
    limits_reached.append(data['limits_reached'])
    time_list.append(data['time_list'])
    # same value for both experimental dataset
    STEP = data['STEP']
    KP = data['KP']
    PERT_EVAL_OFFSET = data['PERT_EVAL_OFFSET']

# %% Reordering the data according to the configuration
for i in range(len(loaded_data)):
    force_list_tmp = deepcopy(force_list[i])
    eps_list_tmp = deepcopy(eps_list[i])
    eps_pred_list_tmp = deepcopy(eps_pred_list[i])
    eval_idx_tmp = deepcopy(eval_idx[i])
    time_list_tmp = deepcopy(time_list[i])
    limits_reached_tmp = deepcopy(limits_reached[i])

    sequence_name[i] = format_increment_with_three_figures(sequence_name[i])
    idx_sorted = np.argsort(sequence_name[i])
    force_list[i] = [deepcopy(force_list_tmp[idx]) for idx in idx_sorted]
    eps_list[i] = [deepcopy(eps_list_tmp[idx]) for idx in idx_sorted]
    eval_idx[i] = [deepcopy(eval_idx_tmp[idx]) for idx in idx_sorted]
    time_list[i] = [deepcopy(time_list_tmp[idx]) for idx in idx_sorted]
    eps_pred_list[i] = [deepcopy(eps_pred_list_tmp[idx]) for idx in idx_sorted]
    limits_reached[i] = [deepcopy(limits_reached_tmp[idx]) for idx in idx_sorted]
    sequence_name[i] = sorted(sequence_name[i])

    del force_list_tmp, eps_list_tmp, eval_idx_tmp, time_list_tmp, limits_reached_tmp

# %% all configuration don't have perturbations on the 3 axes
# getting the indexes of incomplete sequences (missing z, or y)
idx_del_list = []
for ii in range(len(loaded_data)):
    idx_del = []
    prev_config_base_name = ""
    for i, sequence_i in enumerate(sequence_name):
    #
        config_base_name = sequence_i[:-1]
        if prev_config_base_name == config_base_name:
            continue

        for j in range(3):
            if i+j < len(sequence_name):
                if not (config_base_name == sequence_name[i+j][:-1]):
                    idx_del.append([i for i, name in enumerate(sequence_name) if config_base_name in name])
                    break
            else:
                idx_del.append([i for i, name in enumerate(sequence_name) if config_base_name in name])
                break

        prev_config_base_name = config_base_name
    idx_del_list.append(idx_del)

for ii, idx_del in enumerate(idx_del_list):
    idx_del_list[ii] = [item for sublist in idx_del for item in sublist]

# %% deleting incomplete data
for ii, idx_del in enumerate(idx_del_list):
    for index in sorted(idx_del, reverse=True):
        del sequence_name[ii][index]
        del force_list[ii][index]
        del eps_list[ii][index]
        del eval_idx[ii][index]
        del time_list[ii][index]
        del eps_pred_list[ii][index]
        del limits_reached[ii][index]
    sequence_name[ii] = np.array(sequence_name[ii])

# %% looking for the configuration intersection of simulated sequence between the data sets
base_sequence_name = []
for sequence in sequence_name:
    base_sequence_name.append([ name[:11] + name[-2:] for name in sequence])

sequence_name_inter = set.intersection(*map(set, base_sequence_name))
idx_list_inter = []

for sequence in base_sequence_name:
    idx_list_inter.append([index for index, name in enumerate(sequence) if name in sequence_name_inter])
# %% extracting the intersection for each data set
for ii, idx_inter in enumerate(idx_list_inter):
    force_list_tmp = deepcopy(force_list[ii])
    eps_list_tmp = deepcopy(eps_list[ii])
    eps_pred_list_tmp = deepcopy(eps_pred_list[ii])
    eval_idx_tmp = deepcopy(eval_idx[ii])
    time_list_tmp = deepcopy(time_list[ii])
    limits_reached_tmp = deepcopy(limits_reached[ii])
    sequence_name_tmp = deepcopy(sequence_name[ii])

    force_list[ii] = [deepcopy(force_list_tmp[idx]) for idx in idx_inter]
    eps_list[ii] = [deepcopy(eps_list_tmp[idx]) for idx in idx_inter]
    eval_idx[ii] = [deepcopy(eval_idx_tmp[idx]) for idx in idx_inter]
    time_list[ii] = [deepcopy(time_list_tmp[idx]) for idx in idx_inter]
    eps_pred_list[ii] = [deepcopy(eps_pred_list_tmp[idx]) for idx in idx_inter]
    limits_reached[ii] = [deepcopy(limits_reached_tmp[idx]) for idx in idx_inter]
    sequence_name[ii] = [deepcopy(sequence_name_tmp[idx]) for idx in idx_inter]

del force_list_tmp, eps_list_tmp, eps_pred_list_tmp, eval_idx_tmp, time_list_tmp, limits_reached_tmp, sequence_name_tmp

# %% making force and epsilon data homogenous
force_list_hom = []
eps_list_hom = []
eps_pred_list_hom = []
time_list_hom = []

for ii in range(len(sequence_name)):
    for j, (force, eps, eps_pred, t) in enumerate(zip(force_list[ii], eps_list[ii], eps_pred_list[ii], time_list[ii])):
        idx_pert = eval_idx[ii][j]-PERT_EVAL_OFFSET
        force_list_hom.append(force[:,idx_pert-250:idx_pert+PERT_EVAL_OFFSET+1250])
        eps_list_hom.append(eps[:3,idx_pert-250:idx_pert+PERT_EVAL_OFFSET+1250])
        eps_pred_list_hom.append(eps_pred[:,idx_pert-250:idx_pert+PERT_EVAL_OFFSET+1250])
        time_list_hom.append(t[idx_pert-250:idx_pert+PERT_EVAL_OFFSET+1250])

force_list_hom = np.array(force_list_hom)
eps_list_hom = np.array(eps_list_hom)
eps_pred_list_hom = np.array(eps_pred_list_hom)
time_list_hom = np.array(time_list_hom)

# %%
sequence_name = np.concatenate((sequence_name[0], sequence_name[1]), axis=0)
limits_reached = np.concatenate((limits_reached[0], limits_reached[1]), axis=0)
eval_idx = np.concatenate((eval_idx[0], eval_idx[1]), axis=0)

# %% Reordering the data according to the configuration
force_list_hom_tmp = deepcopy(force_list_hom)
eps_list_hom_tmp = deepcopy(eps_list_hom)
eps_pred_list_hom_tmp = deepcopy(eps_pred_list_hom)
time_list_hom_tmp = deepcopy(time_list_hom)
eval_idx_tmp = deepcopy(eval_idx)
limits_reached_tmp = deepcopy(limits_reached)

idx_sorted = np.argsort(sequence_name)
force_list_hom = np.array([deepcopy(force_list_hom_tmp[idx]) for idx in idx_sorted])
eps_list_hom = np.array([deepcopy(eps_list_hom_tmp[idx]) for idx in idx_sorted])
time_list_hom = np.array([deepcopy(time_list_hom_tmp[idx]) for idx in idx_sorted])
eps_pred_list_hom = np.array([deepcopy(eps_pred_list_hom_tmp[idx]) for idx in idx_sorted])
limits_reached = np.array([deepcopy(limits_reached_tmp[idx]) for idx in idx_sorted])
eval_idx = np.array([deepcopy(eval_idx_tmp[idx]) for idx in idx_sorted])
sequence_name = np.array(sorted(sequence_name))

del force_list_hom_tmp, eps_list_hom_tmp, eval_idx_tmp, time_list_hom_tmp, limits_reached_tmp
# %% selecting a valid configuration for both direction at random
seq = np.random.choice(sequence_name)
idx_seq = np.where(sequence_name == seq)[0][0]
if seq[-6] == '-':
    seq2 = seq[:-6] + seq[-5:]
else:
    seq2 = seq[:-6] + '-' + seq[-6:]
idx = np.where(sequence_name == seq)[0][0]
idx2 = np.where(sequence_name == seq2)[0][0]
print("{}:{}".format(seq,limits_reached[idx]))
print("{}:{}".format(seq2,limits_reached[idx2]))

# %%
diff_x_list = []
diff_y_list = []
diff_z_list = []
diff_limits_list = []
fig, axs = plt.subplots(3,1,sharex=True)
for ii, (eps_hom, eps_pred_hom) in enumerate(zip(eps_list_hom, eps_pred_list_hom)):
  norm_eps = np.linalg.norm(eps_pred_hom, axis=0)
  norm_pred = np.linalg.norm(eps_hom, axis=0)
  diff = norm_pred - norm_eps
  # diff_red = np.hstack((diff[150:750], diff[2000:]))
  diff_red = np.array(diff)
  if limits_reached[ii]:
    color = 'tab:orange'
    diff_limits_list.append(diff)
  else:
    color = 'tab:blue'
    if 'x' in sequence_name[ii]:
        axs[0].plot(diff_red, color=color, alpha=0.01)
        diff_x_list.append(diff)
    elif 'y' in sequence_name[ii]:
        axs[1].plot(diff_red, color=color, alpha=0.01)
        diff_y_list.append(diff)
    elif 'z' in sequence_name[ii]:
        axs[2].plot(diff_red, color=color, alpha=0.01)
        diff_z_list.append(diff)

diff_x_list = np.array(diff_x_list)
diff_y_list = np.array(diff_y_list)
diff_z_list = np.array(diff_z_list)

titles = ["fx input", "fy input", "fz input"]
m2mm = tck.FuncFormatter(lambda x, pos: '{0:g}'.format(x*1000))
ms2s = tck.FuncFormatter(lambda x, pos: '{0:g}'.format(x/1000))

iqr50 = [] # interquartile range
iqr90 = [] # interdecile range
qrt05 = [] # 
qrt25 = []
qrt75 = []
qrt95 = []
med = []
stat_idx = 2001 # 601

for ii, diff_ax in enumerate([diff_x_list, diff_y_list, diff_z_list]):
  # diff_red_ax = np.hstack((diff_ax[:,150:750], diff_ax[:,2000:]))
  diff_red_ax = np.array(diff_ax)
  median = np.percentile(diff_red_ax, 50, axis=0)
  quart75 = np.percentile(diff_red_ax, 75, interpolation='higher', axis=0)
  quart25 = np.percentile(diff_red_ax, 25, interpolation='lower', axis=0)
  quart95 = np.percentile(diff_red_ax, 95, interpolation='higher', axis=0)
  quart05 = np.percentile(diff_red_ax, 5, interpolation='lower', axis=0)
  axs[ii].plot(median, color='black', linewidth=2, linestyle='dashed')
  # axs[ii].plot(stat_idx, median[stat_idx], '*', color='tab:orange', markersize=10)
  axs[ii].fill_between(list(range(len(diff_red_ax[0]))), quart75, quart25, color='black', alpha=0.4, label='iqe')
  axs[ii].fill_between(list(range(len(diff_red_ax[0]))), quart95, quart05, color='black', alpha=0.2, label='iqe')
  axs[ii].title.set_text(titles[ii])
  axs[ii].yaxis.set_major_formatter(m2mm)
  axs[ii].xaxis.set_major_formatter(ms2s)
  iqr50.append(quart75 - quart25)
  iqr90.append(quart95 - quart05)
  qrt05.append(quart05)
  qrt25.append(quart25)
  med.append(median)
  qrt75.append(quart75)
  qrt95.append(quart95)
  # axs[ii].plot(np.percentile(diff, 75, interpolation='higher', axis=0), color='black', linestyle='dashed')
  # axs[ii].plot(np.percentile(diff, 25, interpolation='lower', axis=0), color='black', linestyle='dashed')

fig.supxlabel("Time (s)")
fig.supylabel("Error (mm)")
fig.suptitle("Estimation error between simulation and prediction")
# %%
med = np.array(med)
iqr50 = np.array(iqr50)
iqr90 = np.array(iqr90)
t = np.arange(0,2.5,1e-3)

print(med[:,stat_idx])
print(iqr50[:,stat_idx])

qrt05 = np.array(qrt05)
qrt25 = np.array(qrt25)
qrt75 = np.array(qrt75)
qrt95 = np.array(qrt95)

# save undersampled data 
t = t[::10]
med = med[:,::10]
qrt25 = qrt25[:,::10]
qrt75 = qrt75[:,::10]
qrt05 = qrt05[:,::10]
qrt95 = qrt95[:,::10]

data = np.vstack((t, med, qrt05, qrt25, qrt75, qrt95))
np.savetxt("simulation_767_configs_all.csv", data.T, delimiter=',', header="time,med_fx,med_fy,med_fz,qrt05_fx,qrt05_fy,qrt05_fz,qrt25_fx,qrt25_fy,qrt25_fz,qrt75_fx,qrt75_fy,qrt75_fz,qrt95_fx,qrt95_fy,qrt95_fz", comments='')

# data = np.vstack((t, med, iqr50, iqr90))
# np.savetxt("simulation_767_configs_all.csv", data.T, delimiter=',', header="time,med_fx,med_fy,med_fz,iqr50_fx,iqr50_fy,iqr50_fz,iqr90_fx,iqr90_fy,iqr90_fz", comments='')


# np.savetxt("simulation_767_configs.csv", med.T, delimiter=',')
# np.savetxt("simulation_767_configs_2.csv", ieq50.T, delimiter=',')
# np.savetxt("simulation_767_configs_3.csv", ieq90.T, delimiter=',')

# %%
