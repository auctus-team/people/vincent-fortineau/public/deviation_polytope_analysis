Experimental details:

Experiments with two panda arms connected through a mechanical interface.
Both robots are endpoint impedance controlled using the torque qp controller (AUCTUS - INRIA).
A 2kg weight is connected after few seconds, then taken out, generating a force step for both robot.
The weight is connected to the endpoint of each robot that is the middle point of the mechanical interface.

Three configurations are tested, the first one is quasi symmetric, the others are not.
For the each configuration, experiments are repeated for different combinations of control gains Kp1 and Kp2, respectively for the endpoint impedance proportionnal gain of panda 1 and panda 2. The damping gain are defined according to the proportional gain to bring critically damped behaviour (2 sqrt(Kp)).

Control gain combinations:
 - Kp1 250, Kp2 250
 - Kp1 250, Kp2 500
 - Kp1 250, Kp2 750
 - Kp1 500, Kp2 250
 - Kp1 500, Kp2 500
 - Kp1 750, Kp2 250
